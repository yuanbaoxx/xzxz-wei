//app.js
App({
  onLaunch: function() {
    var _this = this;
    // 展示本地存储能力
    var logs = wx.getStorageSync('logs') || []
    logs.unshift(Date.now())
    wx.setStorageSync('logs', logs);
    // wx.getUpdateManager 在 1.9.90 才可用，请注意兼容
    const updateManager = wx.getUpdateManager()
    updateManager.onCheckForUpdate(function (res) {
      // 请求完新版本信息的回调
      console.log(res.hasUpdate)
    })
    updateManager.onUpdateReady(function () {
      wx.showModal({
        title: '更新提示',
        content: '新版本已经准备好，是否马上重启小程序？',
        success: function (res) {
          if (res.confirm) {
            // 新的版本已经下载好，调用 applyUpdate 应用新版本并重启
            updateManager.applyUpdate()
          }
        }
      })
    })
    updateManager.onUpdateFailed(function () {
      // 新的版本下载失败
    })
  },
  
  globalData: {
    //部分请求参数
    baseURL: "https://xiangzhangmingren.com/",
    //http://localhost:8080/xiangzhang_up_war/
    //https://up.xiangzhangmingren.com/
    baseURLtest: "https://up.xiangzhangmingren.com/",
    header: {
      'content-type': 'application/json; charset=utf-8',
    },

    //用户
    userLogin: null,
    userProfile: null,

    //宣讲会、企业信息
    companys: null,
    campuses: null,
    campusTalkIdsOfCollection: null,
    companyIdsOfCollection: null,

    //默认简历
    defaultVideoResume: null,
    defaultOnlineResume: null,
    resumeVideos: [],

    //当前浏览的某个简历，这个简历各个模块的信息
    onlineResume: null,
    baseInfo: null,
    computerSkills: null,
    educations: null,
    honorRewards: null,
    internships: null,
    languageSkills: null,
    projectContests: null,
    schoolExperiences: null,
    selfIntroducations: null,
    skillQualities: null,
  }
})