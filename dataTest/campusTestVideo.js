var campusVideos = [
  {
    id: 1,
    video_name: "中国东方航空2019校园招聘云宣讲",
    company_name: "中国东方航空",
    company_logo: "",
    video_url: "http://pn5x8vsrx.bkt.clouddn.com/%E4%B8%AD%E5%9B%BD%E4%B8%9C%E6%96%B9%E8%88%AA%E7%A9%BA2019%E6%A0%A1%E5%9B%AD%E6%8B%9B%E8%81%98.mp4",
    video_cover_url: "",
    startTime: "2019-2-1 16:00",
    recruitmentBrief: "暂时没有数据"
  },
  {
    id: 2,
    video_name: "恒大集团2019校园招聘云宣讲",
    company_name: "恒大集团",
    company_logo: "",
    video_url: "http://pn5x8vsrx.bkt.clouddn.com/%E6%81%92%E5%A4%A7%E9%9B%86%E5%9B%A22019%E6%A0%A1%E5%9B%AD%E6%8B%9B%E8%81%98.mp4",
    video_cover_url: "",
    startTime: "2019-2-1 16:00",
    recruitmentBrief: "暂时没有数据"
  },
  {
    id: 3,
    video_name: "高顿教育2019校园招聘云宣讲",
    company_name: "高顿教育",
    company_logo: "",
    video_url: "http://pn5x8vsrx.bkt.clouddn.com/%E9%AB%98%E9%A1%BF%E6%95%99%E8%82%B22019%E6%A0%A1%E5%9B%AD%E6%8B%9B%E8%81%98.mp4",
    video_cover_url: "",
    startTime: "2019-2-1 16:00",
    recruitmentBrief: "暂时没有数据"
  },
  {
    id: 4,
    video_name: "新东方2019校园招聘云宣讲",
    company_name: "新东方",
    company_logo: "",
    video_url: "http://pn5x8vsrx.bkt.clouddn.com/%E6%96%B0%E4%B8%9C%E6%96%B92019%E6%A0%A1%E5%9B%AD%E6%8B%9B%E8%81%98.mp4",
    video_cover_url: "",
    startTime: "2019-2-1 16:00",
    recruitmentBrief: "暂时没有数据"
  },
  {
    id: 5,
    video_name: "链家2019校园招聘云宣讲",
    company_name: "链家",
    company_logo: "",
    video_url: "http://pn5x8vsrx.bkt.clouddn.com/%E9%93%BE%E5%AE%B62019%E6%A0%A1%E5%9B%AD%E6%8B%9B%E8%81%98.mp4",
    video_cover_url: "",
    startTime: "2019-2-1 16:00",
    recruitmentBrief: "暂时没有数据"
  },
  {
    id: 6,
    video_name: "中国工商银行2019校园招聘云宣讲",
    company_name: "中国工商银行",
    company_logo: "",
    video_url: "http://pn5x8vsrx.bkt.clouddn.com/%E4%B8%AD%E5%9B%BD%E5%B7%A5%E5%95%86%E9%93%B6%E8%A1%8C2019%E6%A0%A1%E5%9B%AD%E6%8B%9B%E8%81%98.mp4",
    video_cover_url: "",
    startTime: "2019-2-1 16:00",
    recruitmentBrief: "暂时没有数据"
  }

]

module.exports = {
  campusVideos: campusVideos,
}