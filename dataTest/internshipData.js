var internshipsData = [
  {
    id: 1,
    name: "京东管培生",
    address: "武汉、长沙、广东",
    time_required: "3天/周",
    education: "本科",
    salary: "100元/天",
    deadline: "2019-3-1",
    description: "这是一份很轻松的工作",
    company: "京东",
    company_logo: "company_logo",
    job_trade: "管理、管培生",
    is_canregular: 1,
    is_hot: 1
  },
  {
    id: 1,
    name: "京东管培生",
    address: "武汉、长沙、广东",
    time_required: "3天/周",
    education: "本科",
    salary: "100元/天",
    deadline: "2019-3-1",
    description: "这是一份很轻松的工作",
    company: "京东",
    company_logo: "company_logo",
    job_trade: "管理、管培生",
    is_canregular: 1,
    is_hot: 1
  }
]

module.exports = {
  internshipsData: internshipsData
}