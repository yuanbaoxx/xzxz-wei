// pages/campus/air-campus-talk-info/air-campus-talk-info.js
Page({

  /**
   * 页面的初始数据
   */
  data: {
    campusTalkId: "",
    AirCampusTalk: null,
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    this.setData({
      campusTalkId: options.id
    })
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {
    
  },

  /**
   * 根据id请求一条空中宣讲会信息
   */
  getCampusTalkById: function(e) {
    wx.showNavigationBarLoading();
    var _this = this;
    wx.request({
      url: '',
      method: '',
      header: "",
      data: {},
      success: function(res) {
        if(res.statusCode == 200) {
          _this.setData({
            AirCampusTalk: res.data,
          })
        }else {
          console.log(res);
          util.go404Page(res.statusCode, res.data);
        }
      },
      fail: function(res) {
        console.log(res);
        util.go404Page(res.statusCode, res.data);
      },
      complete: function(res) {
        wx.hideNavigationBarLoading();
      }
    })
  }

})