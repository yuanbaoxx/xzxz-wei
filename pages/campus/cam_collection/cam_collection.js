// pages/campus/cam_collection/cam_collection.js
var baseURL = getApp().globalData.baseURL;
var campusUtil = require('../campusUtil/CampusUtil.js');
var baseURLtest = getApp().globalData.baseURLtest;
var util = require("../../../utils/util.js");

Page({

  /**
   * 页面的初始数据
   */
  data: {
    userProfileId: "",
    myCampuses: [],
    pageNow: 0,
    pageSize: 100,
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function(options) {
    var _this = this;
    //获取用户数据
    this.setData({
      userProfileId: options.userProfileId,
    })
    var userProfileId = getApp().globalData.userProfile.id;
    var pageSize = _this.data.pageSize;
    _this.findCampusTalkCollection().then((res) => {
      var myCampuses = res;
      _this.setData({
        myCampuses: myCampuses
      })
      getApp().globalData.myCampuses = myCampuses;
      wx.stopPullDownRefresh();
    })
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function() {
    var _this = this;
    var userProfileId = getApp().globalData.userProfile.id;
    var pageSize = _this.data.pageSize;
    _this.findCampusTalkCollection().then((res) => {
      var myCampuses = res;
      _this.setData({
        myCampuses: myCampuses
      })
      getApp().globalData.myCampuses = myCampuses;
      wx.stopPullDownRefresh();
    })
  },

  /**
 * 根据有详细信息的用户分页获取用户收藏的宣讲会
 */
  findCampusTalkCollection: function(e) {
    var _this = this;
    return new Promise(function (resolve, reject) {
      var myCampuses = [];
      var userProfileId = _this.data.userProfileId;
      var pageNow = _this.data.pageNow;
      var pageSize = _this.data.pageSize;
      wx.request({
        method: "GET",
        url: baseURLtest + 'campus_talk_collection/' + userProfileId + "?page=" + pageNow + "&&size=" + pageSize,
        data: {},
        header: getApp().globalData.header,
        success: function (res) {
          if (res.statusCode == 200) {
            myCampuses = res.data;
            for (var k = 0; k < myCampuses.length; k++) {
              myCampuses[k].start_time = util.formatTime(myCampuses[k].start_time - 28800000);
            }
          } else {
            console.log(res);
            go404Page();
          }
        },
        fail: function (res) {
          console.log(res);
          go404Page();
        },
        complete: function (res) {
          resolve(myCampuses);
        }
      })
    })
  },

  //点击取消收藏
  quit_collectOnList: function (event) {
    var _this = this;
    var campusTalkName = event.currentTarget.dataset.campusTalkName;
    var campusTalkSchool = event.currentTarget.dataset.campusTalkSchool;
    var campusTalkStartTime = event.currentTarget.dataset.campusTalkStartTime;
    var campusTalkAddress = event.currentTarget.dataset.campusTalkAddress;
    var userProfileId = _this.data.userProfileId;
    wx.showModal({
      title: '确认取消收藏？',
      content: '取消后将不会收到时间提醒哦',
      success: function(res) {
        if (res.confirm) {
          wx.showNavigationBarLoading();
          //用户点击确认
          wx.request({
            method: "DELETE",
            url: baseURLtest + "campus_talk_collection?userProfileId=" + userProfileId + "&&campusTalkName=" + campusTalkName + "&&campusTalkSchool=" + campusTalkSchool + "&&campusTalkStartTime=" + campusTalkStartTime + "&&campusTalkAddress=" + campusTalkAddress,
            header: getApp().globalData.header,
            data: {},
            success: function(res) {
              if(res.statusCode == 200) {
                //刷新当前页面数据
                var myCampusesOld = _this.data.myCampuses;
                var myCampusesNew = _this.data.myCampuses;
                for (var i = 0; i < myCampusesOld.length; i++) {
                  if (campusTalkName == myCampusesOld[i].name && campusTalkSchool == myCampusesOld[i].school && campusTalkStartTime == myCampusesOld[i].start_time && campusTalkAddress == myCampusesOld[i].address) {
                    myCampusesNew.splice(i, 1);
                  }
                }
                _this.setData({
                  myCampuses: myCampusesNew
                })
                //提示信息
                wx.hideNavigationBarLoading();
                wx.showToast({
                  title: '取消成功',
                })
              }else {
                console.log(res);
                util.go404Page(res.statusCode, res.data);
              }
            },
            fail: function(res) {
              console.log(res);
              util.go404Page(res.statusCode, res.data);
            }
          })
        } else if (res.cancel) {
          //用户点击取消
        }
      }
    })
  },

  //点击进入宣讲会详情页面
  caGoCampusDetail: function(e) {
    wx.navigateTo({
      url: '/pages/campus/campus_info/campus_info?campusTalkId=' + e.currentTarget.dataset.id + "&userProfileId=" + this.data.userProfileId + "&isCollected=true"
    })
  }

})