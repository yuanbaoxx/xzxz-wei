var ncSchoolData = require("schoolForSelect/schoolForSelect.js").jiangxiSchoolData;
var cityArray = require("schoolForSelect/schoolForSelect.js").citySelectItem;
var util = require('../../utils/util.js');
var campusUtil = require('campusUtil/CampusUtil.js');
var baseURL = getApp().globalData.baseURL;
var mapURL = "https://api.map.baidu.com/geocoder/v2/";
var baseURLtest = getApp().globalData.baseURLtest;
var bannerData = require("schoolForSelect/schoolForSelect.js").bannerData;

Page({
  data: {
    menuFixed: false,
    menuTop: 0,
    bannerData: bannerData,

    //部分区域的遮罩背景
    nowrap: false,
    clientHeight: "",
    windowHeight: 0,

    //城市与学校筛选
    cityArray: [], //所有的城市（对象数组）
    city_view: false, //城市选择框
    citySelected: {}, //选中的城市
    school_view: true, //学校筛选框
    school_alive: false, //举办学校激活的样式
    schoolNow: [], //选中的城市的所有学校
    schoolSelected: new Map(), //用户选中的学校
    showSchoolSelected: [], //筛选框下显示你选中了哪些学校

    //时间筛选
    time_alive: false, //宣讲时间激活的样式
    time_view: false, //时间筛选框
    dates: null, //选中的时间

    //当前页面数据
    keyWords: null, //筛选的条件
    dataFlag: "bySchoolList", //当前页面的数据是什么筛选条件下的数据
    pageNow: 0, //当前数据的页面
    pageSize: 15, //每页的数据条数
    maxPage: false, //是否到达最大页面
    campuses: [], //当前页面的宣讲会（一页一页放进数组里）
    loading: true, //“正在加载中...”的字样的显示
    haveMore: true, //“没有更多了...”字样的显示
    daysOfHaveCampus: new Map(), //有宣讲会的日期Map<month, days[]>
    days_color: [], //有宣讲会的时间的样式集合

    //用户信息与数据
    isLogin: false, //用户是否登录
    haveInfo: false, //用户是否有填简介信息
    campusTalkIdsOfCollection: [], //用收藏的宣讲会id
    haveLocation: true, //是否有定位
  },

  // 2.监听页面滚动距离scrollTop
  onPageScroll: function(e) {
    if (this.data.menuFixed === (e.scrollTop > this.data.menuTop)) return;
    // 3.当页面滚动距离scrollTop > menuTop菜单栏距离文档顶部的距离时，菜单栏固定定位
    this.setData({
      menuFixed: (e.scrollTop > this.data.menuTop)
    })
  },

  onLoad: function(options) {
    var _options = options;
    var _this = this;

    var query = wx.createSelectorQuery()
    query.select('#affix').boundingClientRect();
    query.selectViewport().scrollOffset();
    query.exec(function(res) {
      _this.setData({
        menuTop: res[0].top
      })
    })

    wx.getSystemInfo({
      success: function (res) {
        _this.setData({
          windowHeight: res.windowHeight,
        })
      },
    })
    wx.showLoading({
      title: '',
    })
    //获取有宣讲会的日期并设置日历样式
    this.getDayOfHaveCampus();
    //获取城市位置，并请求信息
    wx.getSetting({
      success: function (res) {
        if (res.authSetting['scope.userLocation'] == false) {
          //用户已经拒绝，需要重新获取
          _this.setData({
            haveLocation: false,
          })
        } else if (res.authSetting['scope.userLocation'] || res.authSetting['scope.userLocation'] == undefined) {
          //用户第一次进入或者已经同意，直接调用接口，
          wx.getLocation({
            success: function (res) {
              //获取定位成功
              var latitude = res.latitude;
              var longitude = res.longitude;
              util.getMyCity(latitude, longitude, mapURL).then((res) => {
                //获取城市成功
                var addressComponent = res.data.result.addressComponent;
                var city = addressComponent.province.replace("省", ''); //替换省市字样
                city = city.replace("市", "");
                var citySelected = cityArray[0]; //选中的城市，默认北京
                for (var item of cityArray) { //根据定位匹配该省的信息
                  if (item.value == city) {
                    citySelected = item;
                    break;
                  }
                }
                var schoolNow = citySelected.schools; //获取该省的学校
                schoolNow[0].selected = true; //设置第一个选项（全部高校）被选中
                var schoolSelected = new Map().set(schoolNow[0].id, schoolNow[0].value);
                _this.setData({
                  citySelected: citySelected,
                  schoolNow: schoolNow,
                  schoolSelected: schoolSelected,
                })
                //获取并设置用户详细信息
                //获取并设置用户收藏
                //获取第一页数据
                var campusTalkIdsOfCollection = [];
                _this.getAndSetloginInfo().then(() => {
                  //获取用户收藏
                  if (_this.data.haveInfo) {
                    var userProfileId = getApp().globalData.userProfile.id;
                    campusUtil.findCampusTalkIdsOfCollection(userProfileId).then((res) => {
                      campusTalkIdsOfCollection = res;
                      _this.setData({
                        campusTalkIdsOfCollection: campusTalkIdsOfCollection
                      })
                      getApp().globalData.campusTalkIdsOfCollection = campusTalkIdsOfCollection;
                    })
                  }
                  var search = _options.search;
                  if (search == "search") {
                    var gotCampuses = [];
                    var keyWord = _options.keyWord;
                    var pageNow = _this.data.pageNow;
                    var pageSize = _this.data.pageSize;
                    campusUtil.getCampus(keyWord, "keyWord", pageNow, pageSize).then((res) => {
                      gotCampuses = res;
                      gotCampuses = campusUtil.addCollectedStatus(gotCampuses, campusTalkIdsOfCollection);
                      _this.setData({
                        campuses: _this.data.campuses.concat(gotCampuses),
                        pageNow: 0,
                        maxPage: false,
                        loading: false,
                        haveMore: false,
                        dataFlag: "keyWord",
                        keyWords: keyWord,
                      })
                      wx.hideLoading();
                    })
                  } else { //非搜索页的跳转
                    _this.getAndSetFirstPageCampusTalk();
                  }
                })
              })
            },
            fail: function(res) {
              var citySelected = cityArray[0];
              var schoolNow = citySelected.schools; //获取该省的学校
              schoolNow[0].selected = true; //设置第一个选项（全部高校）被选中
              var schoolSelected = new Map().set(schoolNow[0].id, schoolNow[0].value);
              _this.setData({
                citySelected: citySelected,
                schoolNow: schoolNow,
                schoolSelected: schoolSelected,
              })
              //获取并设置用户详细信息
              //获取并设置用户收藏
              //获取第一页数据
              var campusTalkIdsOfCollection = [];
              _this.getAndSetloginInfo().then(() => {
                //获取用户收藏
                if (_this.data.haveInfo) {
                  var userProfileId = getApp().globalData.userProfile.id;
                  campusUtil.findCampusTalkIdsOfCollection(userProfileId).then((res) => {
                    campusTalkIdsOfCollection = res;
                    _this.setData({
                      campusTalkIdsOfCollection: campusTalkIdsOfCollection
                    })
                    getApp().globalData.campusTalkIdsOfCollection = campusTalkIdsOfCollection;
                  })
                }
                var search = _options.search;
                if (search == "search") {
                  var gotCampuses = [];
                  var keyWord = _options.keyWord;
                  var pageNow = _this.data.pageNow;
                  var pageSize = _this.data.pageSize;
                  campusUtil.getCampus(keyWord, "keyWord", pageNow, pageSize).then((res) => {
                    gotCampuses = res;
                    gotCampuses = campusUtil.addCollectedStatus(gotCampuses, campusTalkIdsOfCollection);
                    _this.setData({
                      campuses: _this.data.campuses.concat(gotCampuses),
                      pageNow: 0,
                      maxPage: false,
                      loading: false,
                      haveMore: false,
                      dataFlag: "keyWord",
                      keyWords: keyWord,
                    })
                    wx.hideLoading();
                  })
                } else { //非搜索页的跳转
                  _this.getAndSetFirstPageCampusTalk();
                }
              })
            }
          })
        }
      }
    })

    //筛选框数据设置
    var schoolSelected = new Map();
    this.setData({
      cityArray: cityArray,
      citySelected: {
        value: ''
      },
      schoolSelected: schoolSelected
    })
    wx.getSystemInfo({
      success: function (res) {
        _this.setData({
          nowrap: false
        })
      }
    })
    wx.setNavigationBarTitle({
      title: '宣讲会',
    })
  },

  //监听页面显示
  onShow: function () {
    var _this = this;
    _this.getAndSetloginInfo().then(() => {
      //获取用户收藏
      if (_this.data.haveInfo) {
        var userProfileId = getApp().globalData.userProfile.id;
        campusUtil.findCampusTalkIdsOfCollection(userProfileId).then((res) => {
          _this.setData({
            campusTalkIdsOfCollection: res
          })
          getApp().globalData.campusTalkIdsOfCollection = res;
          var campuses = campusUtil.addCollectedStatus(_this.data.campuses, res)
          _this.setData({
            campuses: campuses
          })

        })
      }
    })
  },

  //监听用户下拉动作
  onPullDownRefresh: function () {
    var _this = this;
    var campuses = _this.data.campuses;
    //先判断是否要请求热门数据
    var hotCampuses = [];
    _this.setData({
      campuses: [],
      loading: true,
      haveMore: true,
    })
    wx.showLoading({
      title: '',
    })
    //获取标志，请求参数和页面数据
    var schoolList = _this.data.schoolSelected;
    var datetime = _this.data.dates;
    var flag = this.data.dataFlag;
    var pageNow = 0;
    var pageSize = this.data.pageSize;
    var keyWords = _this.data.keyWords
    //开始处理选中的学校
    var schoolNow = _this.data.schoolNow;
    if (schoolList.get(schoolNow[0].id)) { //有全部高校选项
      for (var i = 0; i < schoolNow.length; i++) {
        schoolList.set(schoolNow[i].id, schoolNow[i].value);
      }
      _this.setData({
        schoolSelected: new Map().set(schoolNow[0].id, schoolNow[0].value),
        keyWords: schoolList
      })
    }
    if (flag == "bySchoolAndTime") {
      //请求热门数据
      campusUtil.getHotCampusBySchoolsTime(schoolList, datetime).then((res) => {
        hotCampuses = res;
        for (var k = 0; k < hotCampuses.length; k++) {
          hotCampuses[k].startTime = util.formatTime(hotCampuses[k].startTime);
        }
        //请求普通数据
        campusUtil.getCampusBySchoolsTime(schoolList, datetime, pageNow, pageSize).then((res) => {
          var gotCampuses = res;
          //增加收藏状态
          var campusTalkIdsOfCollection = _this.data.campusTalkIdsOfCollection;
          gotCampuses = campusUtil.addCollectedStatus(gotCampuses, campusTalkIdsOfCollection);
          _this.setData({
            campuses: hotCampuses.concat(gotCampuses),
            pageNow: 0,
            maxPage: false,
            loading: false,
            dataFlag: flag,
          })
          wx.stopPullDownRefresh();
        })
        wx.hideLoading();
      })
    } else {
      //请求热门数据
      campusUtil.getHotCampusTalkBySchools(schoolList).then((res) => {
        hotCampuses = res;
        for (var k = 0; k < hotCampuses.length; k++) {
          hotCampuses[k].startTime = util.formatTime(hotCampuses[k].startTime);
        }
        //请求普通数据
        campusUtil.getCampus(keyWords, flag, pageNow, pageSize).then((res) => {
          var gotCampuses = res;
          //增加收藏状态
          var campusTalkIdsOfCollection = _this.data.campusTalkIdsOfCollection;
          gotCampuses = campusUtil.addCollectedStatus(gotCampuses, campusTalkIdsOfCollection);
          _this.setData({
            campuses: hotCampuses.concat(gotCampuses),
            pageNow: 0,
            maxPage: false,
            loading: false,
            dataFlag: flag,
          })
          wx.stopPullDownRefresh();
          wx.hideLoading();
        })
      })
    }
  },

  //页面上拉触底
  onReachBottom: function () {
    var _this = this;
    //页面加一
    if (!_this.data.maxPage) {
      this.setData({
        pageNow: (this.data.pageNow + 1),
        loading: true,
        haveMore: true
      })
      //获取标志，请求参数和页面数据
      var schoolList = _this.data.schoolSelected;
      var datetime = _this.data.dates;
      var flag = this.data.dataFlag;
      var pageNow = this.data.pageNow;
      var pageSize = this.data.pageSize;
      if (flag == "bySchoolAndTime") {
        campusUtil.getCampusBySchoolsTime(schoolList, datetime, pageNow, pageSize).then((res) => {
          if (res == "" || res == null || res.length == 0) {
            _this.setData({
              maxPage: true,
              loading: false,
              haveMore: false
            })
          } else {
            var gotCampuses = res;
            //增加收藏状态
            var campusTalkIdsOfCollection = _this.data.campusTalkIdsOfCollection;
            gotCampuses = campusUtil.addCollectedStatus(gotCampuses, campusTalkIdsOfCollection);
            _this.setData({
              campuses: _this.data.campuses.concat(gotCampuses),
              loading: false,
              dataFlag: flag,
              maxPage: false
            })
          }
        })
      } else {
        var keyWords = _this.data.keyWords
        campusUtil.getCampus(keyWords, flag, pageNow, pageSize).then((res) => {
          var gotCampuses = res;
          if (res == "" || res == null || res.length == 0) {
            _this.setData({
              maxPage: true,
              loading: false,
              haveMore: false
            })
          } else {
            //增加收藏状态
            var campusTalkIdsOfCollection = _this.data.campusTalkIdsOfCollection;
            gotCampuses = campusUtil.addCollectedStatus(gotCampuses, campusTalkIdsOfCollection);
            _this.setData({
              campuses: _this.data.campuses.concat(gotCampuses),
              loading: false,
              dataFlag: flag,
              maxPage: false,
            })
          }
        })
      }
    } else {
      this.setData({
        loading: false,
        haveMore: false
      })
    }
  },

  //用户点击右上角分享
  onShareAppMessage: function () {
    var pages = getCurrentPages();
    var page = pages[pages.length - 1];
    var url = page.route;
    return {
      title: "香樟明人-宣讲会",
      path: url
    }
  },

  //第一次拒绝后，每次进入页面重新打开设置
  openSetting: function (res) {
    var _this = this;
    _this.setData({
      haveLocation: true,
    })
    wx.openSetting({
      success: function (res) {
        wx.getLocation({
          success: function (res) {
            var latitude = res.latitude;
            var longitude = res.longitude;
            var citySelected = cityArray[0];
            util.getMyCity(latitude, longitude, mapURL).then((res) => {
              var addressComponent = res.data.result.addressComponent;
              var city = addressComponent.province.replace("省", '');
              city = city.replace("市", "");
              for (var item of cityArray) {
                if (item.value == city) {
                  citySelected = item;
                  break;
                }
              }
              var schoolNow = citySelected.schools;
              schoolNow[0].selected = true;
              var schoolSelected = new Map().set(schoolNow[0].id, schoolNow[0].value);
              _this.setData({
                citySelected: citySelected,
                schoolNow: schoolNow,
                schoolSelected: schoolSelected,
              })
              //获取并设置用户详细信息
              //获取并设置用户收藏
              //获取第一页数据
              var campusTalkIdsOfCollection = [];
              _this.getAndSetloginInfo().then(() => {
                //获取用户收藏
                if (_this.data.haveInfo) {
                  var userProfileId = getApp().globalData.userProfile.id;
                  campusUtil.findCampusTalkIdsOfCollection(userProfileId).then((res) => {
                    campusTalkIdsOfCollection = res;
                    _this.setData({
                      campusTalkIdsOfCollection: campusTalkIdsOfCollection
                    })
                    getApp().globalData.campusTalkIdsOfCollection = campusTalkIdsOfCollection;
                  })
                }
                //请求宣讲会数据
                _this.getAndSetFirstPageCampusTalk();
              })
            })
          },
          fail: function (res) {
            var citySelected = cityArray[0];
            var schoolNow = citySelected.schools; //获取该省的学校
            schoolNow[0].selected = true; //设置第一个选项（全部高校）被选中
            var schoolSelected = new Map().set(schoolNow[0].id, schoolNow[0].value);
            _this.setData({
              citySelected: citySelected,
              schoolNow: schoolNow,
              schoolSelected: schoolSelected,
            })
            //获取并设置用户详细信息
            //获取并设置用户收藏
            //获取第一页数据
            var campusTalkIdsOfCollection = [];
            _this.getAndSetloginInfo().then(() => {
              //获取用户收藏
              if (_this.data.haveInfo) {
                var userProfileId = getApp().globalData.userProfile.id;
                campusUtil.findCampusTalkIdsOfCollection(userProfileId).then((res) => {
                  campusTalkIdsOfCollection = res;
                  _this.setData({
                    campusTalkIdsOfCollection: campusTalkIdsOfCollection
                  })
                  getApp().globalData.campusTalkIdsOfCollection = campusTalkIdsOfCollection;
                })
              }
              //请求宣讲会数据
              _this.getAndSetFirstPageCampusTalk();
            })
          }
        })
      }
    })
  },

  //第一次拒绝后，重新请求后再次拒绝授权
  openSetting_cancel: function (e) {
    var _this = this;
    _this.setData({
      haveLocation: true,
    })
    var citySelected = cityArray[0];
    var schoolNow = citySelected.schools; //获取该省的学校
    schoolNow[0].selected = true; //设置第一个选项（全部高校）被选中
    var schoolSelected = new Map().set(schoolNow[0].id, schoolNow[0].value);
    _this.setData({
      citySelected: citySelected,
      schoolNow: schoolNow,
      schoolSelected: schoolSelected,
    })
    //获取并设置用户详细信息
    //获取并设置用户收藏
    //获取第一页数据
    var campusTalkIdsOfCollection = [];
    _this.getAndSetloginInfo().then(() => {
      //获取用户收藏
      if (_this.data.haveInfo) {
        var userProfileId = getApp().globalData.userProfile.id;
        campusUtil.findCampusTalkIdsOfCollection(userProfileId).then((res) => {
          campusTalkIdsOfCollection = res;
          _this.setData({
            campusTalkIdsOfCollection: campusTalkIdsOfCollection
          })
          getApp().globalData.campusTalkIdsOfCollection = campusTalkIdsOfCollection;
        })
      }
      //请求宣讲会数据
      _this.getAndSetFirstPageCampusTalk();
    })
  },

  //搜索框获得焦点
  wxSerchFocus: function (e) {
    wx.navigateTo({
      url: 'search_page_campus/search_page_cam',
    })
  },

  //点击进入宣讲会详情界面
  caGoCampusDetail: function (event) {
    wx.navigateTo({
      url: '/pages/campus/campus_info/campus_info?campusTalkId=' + event.currentTarget.dataset.campusTalkId + "&&isCollected=" + event.currentTarget.dataset.isCollected
    })
  },

  //获取并设置用户登录状态与信息
  getAndSetloginInfo: function (e) {
    var _this = this;
    return new Promise(function (resolve, reject) {
      wx.showNavigationBarLoading();
      wx.getStorage({
        key: 'userLogin',
        success: function (res) {
          //设置登录状态
          _this.setData({
            isLogin: true
          })
          //设置全局用户数据
          getApp().globalData.userLogin = res.data;
          //请求详细信息,设置信息状态
          var userLoginId = getApp().globalData.userLogin.id;
          campusUtil.getUserProfile(userLoginId).then((res) => {
            _this.setData({
              haveInfo: res ? true : false
            })
            getApp().globalData.userProfile = res;
            wx.hideNavigationBarLoading();
            resolve();
          })
        },
        fail: function (res) {
          _this.setData({
            isLogin: false
          })
          wx.hideNavigationBarLoading();
          resolve();
        },
      })
    })
  },

  //得到页面第一次加载时的宣讲会数据，根据选中的选中的学校查询并设置宣讲会
  getAndSetFirstPageCampusTalk: function (res) {
    var _this = this;
    var gotCampuses = [];
    var pageNow = _this.data.pageNow; //当前页码
    var pageSize = _this.data.pageSize; //当前页码的数据条数
    var schoolNow = _this.data.schoolNow; //当前城市的学校数组
    var showSchoolSelected = [];
    showSchoolSelected.push(schoolNow[0].value); //展示选中的学校，默认第一个选项
    var schoolList = new Map(); //选中的学校，默认所有学校
    schoolList = _this.data.schoolSelected;
    for (var i = 0; i < schoolNow.length; i++) {
      schoolList.set(schoolNow[i].id, schoolNow[i].value);
    }
    _this.setData({
      schoolSelected: new Map().set(schoolNow[0].id, schoolNow[0].value),
      pageNow: 0,
      maxPage: false,
      dataFlag: "bySchoolList",
      schoolNow: schoolNow,
      showSchoolSelected: showSchoolSelected,
      keyWords: schoolList,
    })
    //先获取热门宣讲会
    campusUtil.getHotCampusTalkBySchools(schoolList).then((res) => {
      var hotCampuses = res;
      //请求普通数据(当前地区的全部学校，无时间，根据学校请求数据)
      campusUtil.getCampus(schoolList, "bySchoolList", pageNow, pageSize).then((res) => {
        gotCampuses = res;
        gotCampuses = hotCampuses.concat(gotCampuses);
        gotCampuses = campusUtil.addCollectedStatus(gotCampuses, _this.data.campusTalkIdsOfCollection);
        _this.setData({
          campuses: _this.data.campuses.concat(gotCampuses),
          loading: false,
          haveMore: false,
        })
      })
      wx.hideLoading();
    })
  },

  //点击举办学校
  shool_view: function () {
    this.setData({
      time_alive: false,
      time_view: false,
      schoolView: !this.data.schoolView,
      school_alive: !this.data.school_alive,
      nowrap: !this.data.schoolView
    })
  },

  //点击当前地区,只是切换视图
  selectcity: function (e) {
    var city_view = !this.data.city_view;
    var school_view = !city_view;
    this.setData({
      city_view: city_view,
      school_view: school_view,
    })
  },

  //点击某一个地区(并清空之前选的学校，选中整个地区的宣讲会，出现学校筛选框)
  cityClick: function (e) {
    var schoolSelected = this.data.schoolSelected;
    schoolSelected.clear(); //清空之前选的学校

    var cityItem = e.currentTarget.dataset.city;
    var schoolNow = cityItem.schools;
    schoolNow[0].selected = true; //当前地区全部宣讲会选中
    schoolSelected.set(schoolNow[0].id, schoolNow[0].value);
    this.setData({
      citySelected: cityItem,
      city_view: false,
      school_view: true,
      schoolNow: schoolNow,
      schoolSelected: schoolSelected,
    })
  },

  //点击某一个学校(清除地区全部高校的选中状态，改变选中状态，根据装填添加到已选学校数组里)
  select: function (e) {
    var school = e.currentTarget.dataset.school;
    var index = school.index;
    var schoolSelected = this.data.schoolSelected;
    var schoolNow = this.data.schoolNow;
    //判断点击的是否是该地区全部高校
    if (index != 0) { //不是全部高校，将0不选中，继续这个按钮的操作
      schoolNow[0].selected = false;
      schoolSelected.delete(schoolNow[0].id);
      schoolNow[index].selected = !schoolNow[index].selected; //将框框设置为选中状态
      if (schoolNow[index].selected) { //将选中的加入到已选中学校数组里去
        schoolSelected.set(schoolNow[index].id, schoolNow[index].value);
      } else {
        schoolSelected.delete(schoolNow[index].id);
      }
      this.setData({
        schoolNow: schoolNow,
        schoolSelected: schoolSelected
      })
    } else { //是全部高校，将其他选项不选中，选中全部
      for (var i = 0; i < schoolNow.length; i++) {
        schoolNow[i].selected = false;
      }
      schoolNow[0].selected = true;
      schoolSelected.clear();
      schoolSelected.set(schoolNow[0].id, schoolNow[0].value);
      this.setData({
        schoolNow: schoolNow,
        schoolSelected: schoolSelected
      })
    }
  },

  //点击清空按钮，选中当前地区全部高校
  selectClear: function (e) {
    var schoolNow = this.data.schoolNow;
    for (var i = 0; i < schoolNow.length; i++) {
      schoolNow[i].selected = false;
    }
    schoolNow[0].selected = true;
    var schoolSelected = new Map().set(schoolNow[0].id, schoolNow[0].value);
    this.setData({
      schoolNow: schoolNow,
      schoolSelected: schoolSelected,
    })
  },

  //点击学校确认按钮，获取宣讲会列表
  selectSave: function (e) {
    //点了确定按钮，进入筛选状态
    var _this = this;
    var schoolList = _this.data.schoolSelected; //把选中的学校处理一下(如果是全部高校，就把当前地区学校都添加到里面去)
    //先存储状态
    var list = [];
    var map = schoolList;
    for (var item of map) {
      list.push(item[1]);
    }
    _this.setData({
      showSchoolSelected: list,
    })
    //开始处理选中的学校
    var schoolNow = _this.data.schoolNow;
    if (schoolList.get(schoolNow[0].id)) { //有全部高校选项
      for (var i = 0; i < schoolNow.length; i++) {
        schoolList.set(schoolNow[i].id, schoolNow[i].value);
      }
      _this.setData({
        schoolSelected: new Map().set(schoolNow[0].id, schoolNow[0].value),
      })
    }

    this.setData({
      school_alive: false,
      schoolView: false,
      nowrap: false,
      //加载状态
      loading: true,
      haveMore: true,
    })
    var date = _this.data.dates;
    var gotCampuses = [];
    var hotCampusTalk = [];
    if (schoolList.size == 0) {
      //没有选择学校
      //时间的两种状态
      if (date == null) {} else {}
    } else {
      //有选择学校
      //时间的两种状态
      if (date == null) {
        //有学校无时间
        _this.setData({
          pageNow: 0,
          keyWords: schoolList,
          dataFlag: "bySchoolList",
          campuses: [],
        })
        var pageSize = _this.data.pageSize;
        //根据学校列表请求热门宣讲会
        campusUtil.getHotCampusTalkBySchools(schoolList).then((res) => {
          hotCampusTalk = res;
          //请求普通宣讲会
          campusUtil.getCampus(schoolList, "bySchoolList", 0, pageSize).then((res) => {
            gotCampuses = res;
            //增加收藏状态
            var campusTalkIdsOfCollection = _this.data.campusTalkIdsOfCollection;
            gotCampuses = campusUtil.addCollectedStatus(gotCampuses, campusTalkIdsOfCollection);
            _this.setData({
              campuses: hotCampusTalk.concat(gotCampuses),
              pageNow: 0,
              maxPage: false,
              loading: false,
              haveMore: false,
            })
          })
        })
      } else {
        //有学校有时间
        _this.setData({
          pageNow: 0,
          keyWords: "",
          dataFlag: "bySchoolAndTime",
          campuses: [],
        })
        var pageSize = _this.data.pageSize;
        //根据学校列表和时间段请求热门宣讲会
        campusUtil.getHotCampusBySchoolsTime(schoolList, date).then((res) => {
          hotCampusTalk = res;
          //请求普通宣讲会
          campusUtil.getCampusBySchoolsTime(schoolList, date, 0, pageSize).then((res) => {
            gotCampuses = res;
            //增加收藏状态
            var campusTalkIdsOfCollection = _this.data.campusTalkIdsOfCollection;
            gotCampuses = campusUtil.addCollectedStatus(gotCampuses, campusTalkIdsOfCollection);
            _this.setData({
              campuses: hotCampusTalk.concat(gotCampuses),
              pageNow: 0,
              maxPage: false,
              loading: false,
              haveMore: false,
            })
          })
        })
      }
    }
  },

  //点击宣讲时间
  time_view: function () {
    this.setData({
      school_alive: false,
      schoolView: false,
      time_alive: !this.data.time_alive,
      time_view: !this.data.time_view,
      nowrap: !this.data.time_view
    })
  },

  //获得有宣讲会的日期
  getDayOfHaveCampus: function (e) {
    wx.showNavigationBarLoading();
    var _this = this;
    campusUtil.getTimeOfHaveCampus().then((res) => {
      _this.setData({
        daysOfHaveCampus: res,
      })
      var currentMonthCampusTalkDays = [];
      var tempDate = new Date();
      var currentYearMonth = tempDate.getFullYear() + "-" + (tempDate.getMonth() + 1);
      if (res.get(currentYearMonth) != null) {
        currentMonthCampusTalkDays = res.get(currentYearMonth);
      }
      var days_color = [];
      currentMonthCampusTalkDays.forEach(function (value, index, dates) {
        var date = (new Date(value));
        var dateObject = {
          month: 'current',
          day: date.getDate(),
          color: " white",
          background: "#37585f",
        }
        days_color.push(dateObject);
      })
      _this.setData({
        days_color: days_color
      })
      wx.hideNavigationBarLoading();
    })
  },

  //改变月份
  changeMonth: function (e) {
    var currentMonthCampusTalkDays = [];
    var daysOfHaveCampus = this.data.daysOfHaveCampus;
    var currentYearMonth = e.detail.currentYear + "-" + e.detail.currentMonth;
    if (daysOfHaveCampus.get(currentYearMonth) != null) {
      currentMonthCampusTalkDays = daysOfHaveCampus.get(currentYearMonth);
    }
    var days_color = [];
    currentMonthCampusTalkDays.forEach(function (value, index, dates) {
      var date = (new Date(value));
      var dateObject = {
        month: 'current',
        day: date.getDate(),
        color: " white",
        background: "#37585f",
      }
      days_color.push(dateObject);
    })
    this.setData({
      days_color: days_color
    })

  },

  //点击日期组件确定事件
  dayClick: function (e) {
    //有时间
    var _this = this;
    var date = e.detail.year + "-" + e.detail.month + "-" + e.detail.day;
    var schoolList = _this.data.schoolSelected;
    //处理一个地区全部高校的按钮
    var schoolNow = _this.data.schoolNow;
    if (schoolList.get(schoolNow[0].id)) { //有全部高校选项
      for (var i = 0; i < schoolNow.length; i++) {
        schoolList.set(schoolNow[i].id, schoolNow[i].value);
      }
      _this.setData({
        schoolSelected: new Map().set(schoolNow[0].id, schoolNow[0].value),
      })
    }
    var gotCampuses = [];
    var hotCampusTalk = [];
    _this.setData({
      dates: date,
      time_alive: false,
      time_view: false,
      nowrap: !this.data.time_view,
    })
    if (schoolList.size == 0) {
      //有时间无学校
    } else {
      //有时间有学校
      this.setData({
        //数据
        pageNow: 0,
        keyWords: "",
        dataFlag: "bySchoolAndTime",
        campuses: [],
        //加载状态
        loading: true,
        haveMore: true,
      })
      var pageSize = _this.data.pageSize;
      //根据时间和学校请求热门宣讲会
      campusUtil.getHotCampusBySchoolsTime(schoolList, date).then((res) => {
        hotCampusTalk = res;
        //请求普通宣讲会
        campusUtil.getCampusBySchoolsTime(schoolList, date, 0, pageSize).then((res) => {
          gotCampuses = res;
          //增加收藏状态
          var campusTalkIdsOfCollection = _this.data.campusTalkIdsOfCollection;
          gotCampuses = campusUtil.addCollectedStatus(gotCampuses, campusTalkIdsOfCollection);
          _this.setData({
            campuses: hotCampusTalk.concat(gotCampuses),
            pageNow: 0,
            maxPage: false,
            loading: false,
            haveMore: false,
          })
        })
      })
    }
  },

  //点击所有时间
  any_time: function (e) {
    //无时间
    var _this = this;
    var gotCampuses = [];
    var hotCampuses = [];
    var schoolList = _this.data.schoolSelected;
    //处理一个地区全部高校的按钮
    var schoolNow = _this.data.schoolNow;
    if (schoolList.get(schoolNow[0].id)) { //有全部高校选项
      for (var i = 0; i < schoolNow.length; i++) {
        schoolList.set(schoolNow[i].id, schoolNow[i].value);
      }
      _this.setData({
        schoolSelected: new Map().set(schoolNow[0].id, schoolNow[0].value),
      })
    }
    _this.setData({
      dates: null,
      time_alive: false,
      time_view: false,
      nowrap: !this.data.time_view,
    })
    if (schoolList.size == 0) {
      //无时间无学校
    } else {
      //无时间有学校
      this.setData({
        pageNow: 0,
        keyWords: schoolList,
        dataFlag: "bySchoolList",
        campuses: [],
        //加载状态
        loading: true,
        haveMore: true,
      })
      var pageSize = _this.data.pageSize;
      //根据学校请求热门宣讲会
      campusUtil.getHotCampusTalkBySchools(schoolList).then((res) => {
        hotCampuses = res;
        //请求普通宣讲会
        campusUtil.getCampus(schoolList, "bySchoolList", 0, pageSize).then((res) => {
          gotCampuses = res;
          //增加收藏状态
          var campusTalkIdsOfCollection = _this.data.campusTalkIdsOfCollection;
          gotCampuses = campusUtil.addCollectedStatus(gotCampuses, campusTalkIdsOfCollection);
          _this.setData({
            campuses: hotCampuses.concat(gotCampuses),
            pageNow: 0,
            maxPage: false,
            loading: false,
            haveMore: false,
          })
        })
      })
    }
  },

  //点击收藏图标
  collecOnOne: function (e) {
    var event = e;
    var _this = this;
    if (_this.data.isLogin == false) { //未登录
      wx.showModal({
        title: '提示',
        content: '你还没登录哦',
        success: function (res) {
          if (res.confirm) {
            //用户点击确认
            wx.switchTab({
              url: '/pages/my/my',
            })
          } else if (res.cancel) {
            //用户点击取消
          }
        }
      })
    } else if (this.data.isLogin == true) { //已登录
      var haveInfo = _this.data.haveInfo;
      if (this.data.haveInfo == false) {
        wx.showModal({
          title: '提示',
          content: '你还没填写个人简介哦',
          success: function (res) {
            if (res.confirm) {
              //用户点击确认
              wx.navigateTo({
                url: '/pages/my/user_detail/user_detail',
              })
            } else if (res.cancel) {
              //用户点击取消
            }
          }
        })
      } else if (this.data.haveInfo == true) {
        wx.showNavigationBarLoading();
        //判断收藏状态
        //得到用户uid和宣讲会id
        var isCollected = event.currentTarget.dataset.isCollected;
        var userProfileId = getApp().globalData.userProfile.id;
        var campusTalkName = event.currentTarget.dataset.campusTalkName;
        var campusTalkSchool = event.currentTarget.dataset.campusTalkSchool;
        var campusTalkStartTime = event.currentTarget.dataset.campusTalkStartTime;
        var campusTalkAddress = event.currentTarget.dataset.campusTalkAddress;
        if (isCollected) {
          //已收藏，接下来取消
          wx.request({
            method: "DELETE",
            url: baseURLtest + "campus_talk_collection?userProfileId=" + userProfileId + "&&campusTalkName=" + campusTalkName + "&&campusTalkSchool=" + campusTalkSchool + "&&campusTalkStartTime=" + campusTalkStartTime + "&&campusTalkAddress=" + campusTalkAddress,
            header: getApp().globalData.header,
            data: {},
            success: function (res) {
              if (res.statusCode == 200) {
                wx.showToast({
                  title: '取消成功',
                })
                //重新更新页面的收藏
                var campusTalkIdsOfCollection = getApp().globalData.campusTalkIdsOfCollection;
                var campuses = _this.data.campuses;
                for (var i = 0; i < campusTalkIdsOfCollection.length; i++) {
                  if (campusTalkName == campusTalkIdsOfCollection[i].campusTalkName && campusTalkSchool == campusTalkIdsOfCollection[i].campusTalkSchool && campusTalkStartTime == campusTalkIdsOfCollection[i].campusTalkStartTime && campusTalkAddress == campusTalkIdsOfCollection[i].campusTalkAddress) {
                    campusTalkIdsOfCollection.splice(i, 1);
                    break;
                  }
                }
                campusUtil.addCollectedStatus(campuses, campusTalkIdsOfCollection);
                getApp().globalData.campusTalkIdsOfCollection = campusTalkIdsOfCollection;
                _this.setData({
                  campuses: campuses,
                  campusTalkIdsOfCollection: campusTalkIdsOfCollection
                })
              } else {
                console.log(res);
                util.go404Page(res.statusCode, res.data);
              }
            },
            fail: function (res) {
              console.log(res);
              util.go404Page(res.statusCode, res.data);
            },
            complete: function (res) {
              wx.hideNavigationBarLoading();
            }
          })
        } else {
          //未收藏，接下来收藏
          var param = {
            userProfileId: userProfileId,
            campusTalkName: campusTalkName,
            campusTalkSchool: campusTalkSchool,
            campusTalkStartTime: new Date(campusTalkStartTime).getTime() + 28800000,
            campusTalkAddress: campusTalkAddress,
          };
          param = JSON.stringify(param);
          wx.showNavigationBarLoading();
          wx.request({
            method: "POST",
            url: baseURLtest + '/campus_talk_collection',
            header: getApp().globalData.header,
            data: param,
            success: function (res) {
              if (res.statusCode == 200) {
                wx.showToast({
                  title: '收藏成功',
                })
                //重新更新页面的收藏
                var campusTalkIdsOfCollection = getApp().globalData.campusTalkIdsOfCollection;
                var campuses = _this.data.campuses;
                campusTalkIdsOfCollection.push({
                  userProfileId: userProfileId,
                  campusTalkName: campusTalkName,
                  campusTalkSchool: campusTalkSchool,
                  campusTalkStartTime: campusTalkStartTime,
                  campusTalkAddress: campusTalkAddress,
                });
                campusUtil.addCollectedStatus(campuses, campusTalkIdsOfCollection);
                getApp().globalData.campusTalkIdsOfCollection = campusTalkIdsOfCollection;
                _this.setData({
                  campuses: campuses,
                  campusTalkIdsOfCollection: campusTalkIdsOfCollection
                })
              } else {
                console.log(res);
                util.go404Page(res.statusCode, res.data);
              }
            },
            fail: function (res) {
              console.log(res);
              util.go404Page(res.statusCode, res.data);
            },
            complete: function (res) {
              wx.hideNavigationBarLoading();
            }
          })
        }
      }
    }

  },

  //点击我的收藏
  collect: function () {
    var _this = this;
    if (!_this.data.isLogin) { //未登录
      wx.showModal({
        title: '提示',
        content: '你还没登录哦',
        success: function (res) {
          if (res.confirm) {
            //用户点击确认
            wx.switchTab({
              url: '/pages/my/my',
            })
          } else if (res.cancel) {
            //用户点击取消
          }
        }
      })
    } else if (_this.data.isLogin) { //已登录
      var haveInfo = _this.data.haveInfo;
      if (!_this.data.haveInfo) {
        wx.showModal({
          title: '提示',
          content: '你还没填写个人简介哦',
          success: function (res) {
            if (res.confirm) {
              //用户点击确认
              wx.navigateTo({
                url: '/pages/my/user_detail/user_detail',
              })
            } else if (res.cancel) {
              //用户点击取消
            }
          }
        })
      } else if (_this.data.haveInfo) {
        wx.navigateTo({
          url: '/pages/campus/cam_collection/cam_collection?userProfileId=' + getApp().globalData.userProfile.id,
        })
      }
    }
  },

  //banner点击跳转
  bannerOutWeb: function(e) {
    wx.navigateTo({
      url: '/pages/out_web/out_web?web_url=' + encodeURIComponent(JSON.stringify(e.target.dataset.webUrl)) + "&&title=" + e.target.dataset.title,
    })
  },
  test: function(e) {},
})