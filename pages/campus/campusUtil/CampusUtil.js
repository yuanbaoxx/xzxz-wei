var util = require('../../../utils/util.js'); //公共函数
var baseURL = getApp().globalData.baseURL;
var baseURLtest = getApp().globalData.baseURLtest;

/**
 * 1、获取所有宣讲会
 * 2、根据学校列表筛选宣讲会
 * 3、筛选某一天的宣讲会
 * 4、根据宣讲会和公司名称关键词筛选宣讲会
 */
function getCampus(theKeyWords, flag, page, size) {
  return new Promise(function(resolve, reject) {
    var method = "";
    var param = new Object();
    var url = "";
    if (flag == "all") {
      method = "GET";
      url = baseURLtest + 'campus_talk?page=' + page + "&&size=" + size;
    } else if (flag == "bySchoolList") {
      method = "POST";
      url = baseURLtest + "campus_talk/school_normal?page=" + page + "&&size=" + size;
      param = new Array();
      theKeyWords.forEach(function(value, key, theKeyWords) {
        param.push(value);
      })
    } else if (flag == "byDate") {
      method = "GET";
      var begin = theKeyWords + " 00:00:00";
      var end = theKeyWords + " 23:59:59";
      url = baseURLtest + "campus_talk/time?begin=" + begin + "&&end=" + end + "&&page=" + page + "&&size=" + size;
    } else if (flag == "keyWord") {
      method = "GET";
      url = baseURLtest + "campus_talk/key_word?page=" + page + "&&size=" + size + "&&keyWord=" + theKeyWords;
    }
    var gotCampuses = [];
    wx.request({
      method: method,
      url: url,
      header: getApp().globalData.header,
      data: param,
      success: function(res) {
        if (res.statusCode == 200) {
          gotCampuses = res.data;
          for (var k = 0; k < gotCampuses.length; k++) {
            gotCampuses[k].startTime = util.formatTime(gotCampuses[k].startTime-28800000);
          }
        } else {
          console.log(res);
          util.go404Page(res.statusCode, res.data);
        }
      },
      fail: function(res) {
        console.log(res);
        util.go404Page(res.statusCode, res.data);
      },
      complete: function(res) {
        resolve(gotCampuses);
      }
    });
  })
}

/**
 * 根据学校列表查询热门宣讲会
 */
function getHotCampusTalkBySchools(schools) {
  return new Promise(function(resolve, reject) {
    var gotCampusTalk = [];
    var param = new Array();
    schools.forEach(function (value, key, schools) {
      param.push(value);
    })
    wx.request({
      method: "POST",
      header: getApp().globalData.header,
      url: baseURLtest + "campus_talk/school_hot",
      data: param,
      success: function(res) {
        if (res.statusCode == 200) {
          gotCampusTalk = res.data;
          for (var k = 0; k < gotCampusTalk.length; k++) {
            gotCampusTalk[k].startTime = util.formatTime(gotCampusTalk[k].startTime - 28800000);
          }
        } else {
          console.log(res);
          util.go404Page(res.statusCode, res.data);
        }
      },
      fail: function(res) {
        console.log(res);
        util.go404Page(res.statusCode, res.data);
      },
      complete: function(res) {
        resolve(gotCampusTalk);
      }
    })
  })
}

/**
 * 根据学校列表和某天时间查询普通宣讲会
 */
function getCampusBySchoolsTime(schoolList, datetime, page, size) {
  return new Promise(function(resolve, reject) {
    var begin = datetime + " 00:00:00";
    var end = datetime + " 23:59:59";
    var url = baseURLtest + "campus_talk/school_and_time_normal?begin=" + begin + "&&end=" + end + "&&page=" + page + "&&size=" + size;
    var param = new Array();
    schoolList.forEach(function(value, key, schoolList) {
      param.push(value);
    })
    var gotCampuses = [];
    wx.request({
      method: "POST",
      url: url,
      header: getApp().globalData.header,
      data: param,
      success: function(res) {
        if (res.statusCode == 200) {
          gotCampuses = res.data
          for (var k = 0; k < gotCampuses.length; k++) {
            gotCampuses[k].startTime = util.formatTime(gotCampuses[k].startTime - 28800000);
          }
        } else {
          console.log(res);
          util.go404Page(res.statusCode, res.data);
        }
      },
      fail: function(res) {
        console(res);
        util.go404Page(res.statusCode, res.data);
      },
      complete: function(res) {
        resolve(gotCampuses);
      }
    });
  })
}

/**
 * 根据学校列表和时间段查询热门宣讲会
 */
function getHotCampusBySchoolsTime(schoolList, datetime) {
  return new Promise(function (resolve, reject) {
    var begin = datetime + " 00:00:00";
    var end = datetime + " 23:59:59";
    var url = baseURLtest + "campus_talk/school_and_time_hot?begin=" + begin + "&&end=" + end;
    var param = new Array();
    schoolList.forEach(function (value, key, schoolList) {
      param.push(value);
    })
    var gotCampuses = [];
    wx.request({
      method: "POST",
      url: url,
      header: getApp().globalData.header,
      data: param,
      success: function (res) {
        if (res.statusCode == 200) {
          gotCampuses = res.data
          for (var k = 0; k < gotCampuses.length; k++) {
            gotCampuses[k].startTime = util.formatTime(gotCampuses[k].startTime - 28800000);
          }
        } else {
          console.log(res);
          util.go404Page(res.statusCode, res.data);
        }
      },
      fail: function (res) {
        console(res);
        util.go404Page(res.statusCode, res.data);
      },
      complete: function (res) {
        resolve(gotCampuses);
      }
    });
  })
}

/**
 * 根据用户登录的id获取用户的信息
 */
function getUserProfile(userLoginId) {
  return new Promise(function(resolve, reject) {
    var userProfile = new Object();
    wx.request({
      method: "GET",
      url: baseURLtest + "user_profile/" + userLoginId,
      header: getApp().globalData.header,
      data: {},
      success: function(res) {
        if (res.statusCode == 200) {
          userProfile = res.data;
        } else {
          console.log(res);
          util.go404Page(res.statusCode, res.data);
        }
      },
      fail: function(res) {
        console.log(res);
        util.go404Page(res.statusCode, res.data);
      },
      complete: function(res) {
        resolve(userProfile);
      }
    })
  })
}

/**
 * 根据有详细信息的用户分页获取用户收藏的宣讲会
 */
function findCampusTalkCollection(userProfileId, page, size) {
  return new Promise(function(resolve, reject) {
    var myCampuses = [];
    wx.request({
      method: "GET",
      url: baseURLtest + 'campus_talk_collection/' + userProfileId + "?page=" + page + "&&size=" + size,
      data: {},
      header: getApp().globalData.header,
      success: function(res) {
        if (res.statusCode == 200) {
          myCampuses = res.data;
          for (var k = 0; k < myCampuses.length; k++) {
            myCampuses[k].startTime = util.formatTime(myCampuses[k].startTime - 28800000);
          }
        } else {
          console.log(res);
          util.go404Page(res.statusCode, res.data);
        }
      },
      fail: function(res) {
        console.log(res);
        util.go404Page(res.statusCode, res.data);
      },
      complete: function(res) {
        resolve(myCampuses);
      }
    })
  })
}

/**
 * 根据用户信息的id请求用户收藏宣讲会的id
 */
function findCampusTalkIdsOfCollection(userProfileId) {
  return new Promise(function(resolve, reject) {
    var campusTalkIds = [];
    wx.request({
      method: "GET",
      url: baseURLtest + "campus_talk_collection/my_campus_talk_id?userProfileId=" + userProfileId,
      data: {},
      header: getApp().globalData.header,
      success: function(res) {
        if (res.statusCode == 200) {
          var camcData = res.data;
          for(var item of camcData) {
            campusTalkIds.push({
              campusTalkName: item[0],
              campusTalkSchool: item[1],
              campusTalkStartTime: util.formatTime(item[2] - 28800000),
              campusTalkAddress: item[3],
            })
          }
        } else {
          console.log(res);
          util.go404Page(res.statusCode, res.data);
        }
      },
      fail: function(res) {
        console.log(res);
        util.go404Page(res.statusCode, res.data);
      },
      complete: function(res) {
        resolve(campusTalkIds);
      }
    })
  })
}

/**
 * 请求有宣讲会的日期
 */
function getTimeOfHaveCampus() {
  return new Promise(function(resolve, reject) {
    var campusTalkDays = new Map();
    wx.request({
      method: "GET",
      url: baseURLtest + 'campus_talk/List_time',
      header: getApp().globalData.header,
      data: {},
      success: function(res) {
        if (res.statusCode == 200) {
          var gotTimes = res.data;
          for (var i = 0; i < gotTimes.length; i++) {
            var date = new Date(gotTimes[i] - 28800000);
            var YearMonth = date.getFullYear() + "-" +(date.getMonth() + 1);
            if (campusTalkDays.get(YearMonth) == null){
              campusTalkDays.set(YearMonth, []);
            }
            var dateStr = date.getFullYear() + "-" + (date.getMonth() + 1) + "-" + date.getDate();
            if (campusTalkDays.get(YearMonth).indexOf(dateStr) == -1) {
              var arr = campusTalkDays.get(YearMonth);
              arr.push(dateStr);
              campusTalkDays.set(YearMonth, arr);
            }
          }
        } else {
          console.log(res);
          util.go404Page(res.statusCode, res.data);
        }
      },
      fail: function(res) {
        console.log(res);
        util.go404Page(res.statusCode, res.data);
      },
      complete: function(res) {
        resolve(campusTalkDays);
      }
    })
  })
}

/**
 * 宣讲会时间格式转换
 */
function changeTimeModel(campusData) {
  for (var i = 0; i < campusData.length; i++) {
    campusData[i].startTime = util.formatTime(campusData[i].startTime);
  }
  return campusData;
}

/**
 * 给宣讲会添加收藏状态
 */
function addCollectedStatus(campusData, campusTalkIdsOfCollection) {
  for (var i = 0; i < campusData.length; i++) {
    campusData[i].isCollected = false;
    for (var j = 0; j < campusTalkIdsOfCollection.length; j++) {
      if (campusData[i].name == campusTalkIdsOfCollection[j].campusTalkName && campusData[i].school == campusTalkIdsOfCollection[j].campusTalkSchool && campusData[i].startTime == campusTalkIdsOfCollection[j].campusTalkStartTime && campusData[i].address == campusTalkIdsOfCollection[j].campusTalkAddress) {
        campusData[i].isCollected = true;
        break;
      }
    }
  }
  return campusData;
}

module.exports = {
  getCampus: getCampus,
  getCampusBySchoolsTime: getCampusBySchoolsTime,
  addCollectedStatus: addCollectedStatus,
  getUserProfile: getUserProfile,
  changeTimeModel: changeTimeModel,
  findCampusTalkCollection: findCampusTalkCollection,
  getTimeOfHaveCampus: getTimeOfHaveCampus,
  findCampusTalkIdsOfCollection: findCampusTalkIdsOfCollection,
  getHotCampusBySchoolsTime: getHotCampusBySchoolsTime,
  getHotCampusTalkBySchools: getHotCampusTalkBySchools
}