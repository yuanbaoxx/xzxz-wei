var WxParse = require('../../../wxParse/wxParse.js');
var baseURL = getApp().globalData.baseURL;
var baseURLtest = getApp().globalData.baseURLtest;
var util = require("../../../utils/util.js");

Page({

  /**
   * 页面的初始数据
   */
  data: {
    campusId: "",
    detailCampus: "",
    isCollected: false,
    isLogin: false,
    haveInfo: false,
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function(options) {
    var _this = this;
    //时间格式转换函数
    Date.prototype.format = function(format) {
      var o = {
        "M+": this.getMonth() + 1, // month
        "d+": this.getDate(), // day
        "h+": this.getHours(), // hour
        "m+": this.getMinutes(), // minute
        "s+": this.getSeconds(), // second
        "q+": Math.floor((this.getMonth() + 3) / 3), // quarter
        "S": this.getMilliseconds()
        // millisecond
      };
      if (/(y+)/.test(format) || /(Y+)/.test(format)) {
        format = format.replace(RegExp.$1, (this.getFullYear() + "").substr(4 - RegExp.$1.length));
      }
      for (var k in o) {
        if (new RegExp("(" + k + ")").test(format)) {
          format = format.replace(RegExp.$1, RegExp.$1.length == 1 ? o[k] : ("00" + o[k]).substr(("" + o[k]).length));
        }
      }
      return format;
    }
    //获得路径信息
    var isCollected = (options.isCollected != null) ? options.isCollected : false;
    this.setData({
      campusTalkId: options.campusTalkId,
      isCollected: (isCollected == "true") ? true : false,
    });
    //获取登录信息
    this.setData({
      isLogin: (getApp().globalData.userLogin != null) ? true : false,
      haveInfo: (getApp().globalData.userProfile != null) ? true : false,
    })
    WxParse.wxParse('content', 'html', "正在加载中...", _this, 5);
    this.getcampusDetail();
    wx.setNavigationBarTitle({
      title: '宣讲会详情',
    })
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function() {
    WxParse.wxParse('content', 'html', "正在加载中...", this, 5);
    this.getcampusDetail();
    wx.stopPullDownRefresh();
  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function(res) {
    return {
      title: "香樟明人-宣讲会详情",
      path: "/pages/campus/campus_info/campus_info?campusTalkId=" + this.data.campusTalkId
    }
  },

  /**
   * 点击收藏按钮收藏
   */
  collecting: function(event) {
    var _this = this;
    var isCollected = _this.data.isCollected;
    var campusTalkName = event.currentTarget.dataset.campusTalkName;
    var campusTalkSchool = event.currentTarget.dataset.campusTalkSchool;
    var campusTalkStartTime = event.currentTarget.dataset.campusTalkStartTime;
    var campusTalkAddress = event.currentTarget.dataset.campusTalkAddress;
    if (!isCollected) {
      //未收藏,接下来收藏
      if (!_this.data.isLogin) {
        wx.showModal({
          title: '提示',
          content: '你还没登录哦',
          success: function(res) {
            if (res.confirm) {
              //用户点击确认
              wx.switchTab({
                url: '/pages/my/my',
              })
            } else if (res.cancel) {
              //用户点击取消
            }
          }
        })
        return;
      } else if (!_this.data.haveInfo) {
        wx.showModal({
          title: '提示',
          content: '你还没填写个人简介哦',
          success: function(res) {
            if (res.confirm) {
              //用户点击确认
              wx.switchTab({
                url: '/pages/my/user_detail/detail',
              })
            } else if (res.cancel) {
              //用户点击取消
            }
          }
        })
        return;
      }
      wx.showNavigationBarLoading();
      var userProfileId = getApp().globalData.userProfile.id;
      var param = {
        userProfileId: userProfileId,
        campusTalkName: campusTalkName,
        campusTalkSchool: campusTalkSchool,
        campusTalkStartTime: campusTalkStartTime,
        campusTalkAddress: campusTalkAddress,
      }
      param = JSON.stringify(param);
      wx.request({
        method: "POST",
        url: baseURLtest + 'campus_talk_collection',
        header: getApp().globalData.header,
        data: param,
        success: function(res) {
          if(res.statusCode == 200) {
            wx.showToast({
              title: '收藏成功',
            })
            //重新获取收藏，刷新列表
            _this.setData({
              isCollected: true
            })
          }else{
            console.log(res);
            util.go404Page(res.statusCode, res.data);
          }
        },
        fail: function(res){
          console.log(res);
          util.go404Page(res.statusCode, res.data);
        },
        complete: function(res) {
          wx.hideNavigationBarLoading();
        }
      })

    } else if (isCollected) {
      wx.showNavigationBarLoading();
      var userProfileId = getApp().globalData.userProfile.id;
      //已收藏，接下来取消
      wx.showModal({
        title: '确认取消收藏？',
        content: '取消后将不会收到时间提醒哦',
        success: function(res) {
          if (res.confirm) {
            wx.showNavigationBarLoading();
            //用户点击确认
            wx.request({
              method: "DELETE",
              url: baseURLtest + 'campus_talk_collection?userProfileId=' + userProfileId + "&&campusTalkName=" + campusTalkName + "&&campusTalkSchool=" + campusTalkSchool + "&&campusTalkStartTime=" + campusTalkStartTime + "&&campusTalkAddress=" + campusTalkAddress,
              header: getApp().globalData.header,
              data: {},
              success: function(res) {
                if(res.statusCode == 200) {
                  //刷新当前页面数据
                  _this.setData({
                    isCollected: false
                  })
                  //提示信息
                  wx.hideNavigationBarLoading();
                  wx.showToast({
                    title: '取消成功',
                  })
                }else {
                  console.log(res);
                  util.go404Page(res.statusCode, res.data);
                }
              },
              fail: function(res) {
                console.log(res);
                util.go404Page(res.statusCode, res.data);
              },
              complete: function(res) {
                wx.hideNavigationBarLoading();
              }
            })
          } else if (res.cancel) {
            //用户点击取消
          }
        }
      })
    }
  },

  //获取宣讲会详情
  getcampusDetail: function() {
    var _this = this;
    wx.showLoading({
      title: '正在加载中',
    })
    wx.request({
      method: "GET",
      url: baseURLtest + 'campus_talk/' + _this.data.campusTalkId,
      data: {},
      header: getApp().globalData.header,
      success: function(res) {
        if(res.statusCode == 200) {
          var detailCampusData = res.data;
          detailCampusData.startTime = (new Date(parseFloat(res.data.startTime - 28800000))).format("yyyy-MM-dd hh:mm:ss");
          WxParse.wxParse('content', 'html', detailCampusData.content, _this, 5);
          _this.setData({
            detailCampus : detailCampusData,
          })
        }else {
          console.log(res);
          util.go404Page(res.statusCode, res.data);
        }
      },
      fail: function(res) {
        console.log(res);
        util.go404Page(res.statusCode, res.data);
      },
      complete: function(res) {
        wx.hideLoading();
      }
    })
  },
})