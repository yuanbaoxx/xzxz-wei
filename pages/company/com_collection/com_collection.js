// pages/company/com_collection/com_collection.js
var baseURL = getApp().globalData.baseURL;
var companyUtil = require('../companyUtil/companyUtil.js');
var util = require("../../../utils/util.js");
var baseURLtest = getApp().globalData.baseURLtest;

Page({

  /**
   * 页面的初始数据
   */
  data: {
    userProfileId: null,
    myCompanies: [],
    pageNow: 0,
    pageSize: 15,
    maxPage: false,
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function(options) {
    //获取数据
    companyUtil.findCompaniesCollection(options.userProfileId, 0, this.data.pageSize).then((res) => {
      this.setData({
        userProfileId: options.userProfileId,
        myCompanies: res
      })
    })

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function() {
    var _this = this;
    _this.setData({
      myCompanies: [],
    })
    var userProfileId = getApp().globalData.userProfileId.id;
    companyUtil.getCollection(userProfileId, 0, _this.data.pageSize).then((res) => {
      _this.setData({
        myCompanies: res,
        pageNow: 0,
        pageSize: 15,
        maxPage: false
      })
      wx.stopPullDownRefresh();
    })
  },

  //点击取消收藏
  quit_collectOnList: function(e) {
    var _this = this;
    var companyId = e.currentTarget.dataset.companyId;
    var userProfileId = _this.data.userProfileId;
    wx.showModal({
      title: '确认取消收藏？',
      content: '取消后将不会收到信息提醒哦',
      success: function(res) {
        if (res.confirm) {
          wx.showNavigationBarLoading();
          //用户点击确认
          var param = {
            userProfileId: userProfileId,
            companyId: companyId
          };
          param = JSON.stringify(param);
          wx.request({
            method: "DELETE",
            url: baseURLtest + "company_collection?companyId=" + companyId + "&userProfileId=" + userProfileId,
            header: getApp().globalData.header,
            data: param,
            success: function(res) {
              if(res.statusCode == 200) {
                //刷新当前页面数据
                var myCompaniesOld = _this.data.myCompanies;
                var myCompaniesNew = _this.data.myCompanies;
                for (var i = 0; i < myCompaniesOld.length; i++) {
                  if (myCompaniesOld[i].id == companyId) {
                    myCompaniesNew.splice(i, 1);
                  }
                }
                _this.setData({
                  myCompanies: myCompaniesNew
                })
                //提示信息
                wx.showToast({
                  title: '取消成功',
                })
              }else{
                console.log(res);
                util.go404Page(res.statusCode, res.data);
              }
            },
            fail: function(res) {
              console.log(res);
              util.go404Page(res.statusCode, res.data);
            },
            complete: function(res) {
              wx.hideNavigationBarLoading();
            }
          })
        } else if (res.cancel) {
          //用户点击取消
        }
      }
    })
  },

  //点击进入企业详情页面
  goCompanyWork: function(e) {
    wx.navigateTo({
      url: '/pages/company/company_work/company_work?companyId=' + e.currentTarget.dataset.companyId,
    })
  }

})