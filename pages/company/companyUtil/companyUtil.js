var util = require('../../../utils/util.js'); //公共函数
var baseURL = getApp().globalData.baseURL;
var baseURLtest = getApp().globalData.baseURLtest;

/**
 * 根据条件分页请求公司列表
 * 1、所有公司
 * 2、根据行业筛选公司
 * 3、根据名字关键词筛选公司
 */
function getCompanies(theKeyWords, flag, pageNow, pageSize) {
  return new Promise(function(resolve, reject) {
    var method = "GET";
    var param = new Object();
    var url = "";
    if (flag == "all") {
      url = baseURLtest + "company?page=" + pageNow + "&&size=" + pageSize;
    } else if (flag == "byTrade") {
      url = baseURLtest + "company/trade?page=" + pageNow + "&&size=" + pageSize;
      method = "POST";
      param = theKeyWords;
    } else if (flag == "byKeyWord") {
      url = baseURLtest + "company/name?name=" + theKeyWords + "&&page=" + pageNow + "&&size=" + pageSize;
    }
    var gotCompanies = [];
    wx.request({
      method: method,
      url: url,
      header: getApp().globalData.header,
      data: param,
      success: function(res) {
        if (res.statusCode == 200) {
          gotCompanies = res.data;
        } else {
          console.log(res);
          util.go404Page(res.statusCode, res.data);
        }
      },
      fail: function(res) {
        console.log(res);
        util.go404Page(res.statusCode, res.data);
      },
      complete: function(res) {
        resolve(gotCompanies);
      }
    })
  })
}

/**
 * 根据用户基本信息主键分页请求用户的收藏公司
 */
function findCompaniesCollection(userProfileId, pageNow, pageSize) {
  return new Promise(function(resolve, reject) {
    var myCompanies = [];
    wx.request({
      method: "GET",
      url: baseURLtest + "company_collection/" + userProfileId + "?page=" + pageNow + "&&size=" + pageSize,
      data: {},
      header: getApp().globalData.header,
      success: function(res) {
        if (res.statusCode == 200) {
          myCompanies = res.data;
        } else {
          util.go404Page(res.statusCode, res.data);
        }
      },
      fail: function(res) {
        console.log(res);
        util.go404Page(res.statusCode, res.data);
      },
      complete: function(res) {
        resolve(myCompanies);
      }
    })
  })
}

/**
 * 根据用户基本信息主键请求用户所收藏企业的id
 */
function findCompanyIdsOfCollection(userProfileId) {
  return new Promise(function(resolve, reject) {
    var companyIds = [];
    wx.request({
      method: "GET",
      url: baseURLtest + "company_collection/my_company_id?userProfileId=" + userProfileId,
      data: {},
      header: getApp().globalData.header,
      success: function(res) {
        if (res.statusCode == 200) {
          companyIds = res.data;
        } else {
          console.log(res);
          util.go404Page(res.statusCode, res.data);
        }
      },
      fail: function(res) {
        console.log(res);
        util.go404Page(res.statusCode, res.data);
      },
      complete: function(res) {
        resolve(companyIds);
      }
    })
  })
}

/**
 * 请求热门企业
 */
function getHotCompanies() {
  return new Promise(function(resolve, reject) {
    var hotCompanies = [];
    wx.request({
      method: "GET",
      url: baseURLtest + 'company/hot',
      header: getApp().globalData.header,
      data: {},
      success: function(res) {
        if (res.statusCode == 200) {
          hotCompanies = res.data;
        } else {
          console.log(res);
          util.go404Page(res.statusCode, res.data);
        }
      },
      fail: function(res) {
        console.log(res);
        util.go404Page(res.statusCode, res.data);
      },
      complete: function(res) {
        resolve(hotCompanies);
      }
    })
  })
}

/**
 * 根据companyId获取一条宣讲会
 */
function getCompanyDetailByCompanyId(companyId) {
  return new Promise(function(resolve, reject) {
    var companyDetail = new Object();
    wx.request({
      method: "GET",
      url: baseURLtest + 'company/' + companyId,
      data: {},
      header: getApp().globalData.header,
      success: function(res) {
        if (res.statusCode == 200) {
          companyDetail = res.data;
        } else {
          console.log(res);
          util.go404Page(res.statusCode, res.data);
        }
      },
      fail: function(res) {
        console.log(res);
        util.go404Page(res.statusCode, res.data);
      },
      complete: function(res) {
        resolve(companyDetail);
      }
    })
  })
}

/**
 * 根据公司名字精准查询职位
 */
function listPositionsByCompanyName(companyName) {
  return new Promise(function(resolve, reject) {
    var positionsOfCompany = [];
    wx.request({
      method: "GET",
      url: baseURLtest + "position/list/" + companyName,
      data: {},
      header: getApp().globalData.header,
      success: function (res) {
        if(res.statusCode == 200) {
          positionsOfCompany = res.data;
        }else{
          console.log(res);
          util.go404Page(res.statusCode, res.data);
        }
      },
      fail: function(res) {
        console.log(res);
        util.go404Page(res.statusCode, res.data);
      },
      complete: function(res) {
        resolve(positionsOfCompany);
      }
    })
  })
}

/**
 * 给宣讲会添加收藏状态
 */
function addCollectedStatus(companyData, companyIdsOfCollection) {
  for (var i = 0; i < companyData.length; i++) {
    companyData[i].isCollected = false;
    for (var j = 0; j < companyIdsOfCollection.length; j++) {
      if (companyData[i].id == companyIdsOfCollection[j]) {
        companyData[i].isCollected = true;
        break;
      }
    }
  }
  return companyData;
}

/**
 * 根据用户登录的id获取用户的信息
 */
function getUserProfile(userLoginId) {
  return new Promise(function(resolve, reject) {
    var userProfile = new Object();
    wx.request({
      method: "GET",
      url: baseURLtest + "user_profile/" + userLoginId,
      header: getApp().globalData.header,
      data: {},
      success: function(res) {
        if (res.statusCode == 200) {
          userProfile = res.data;
        } else {
          console.log(res);
          util.go404Page(res.statusCode, res.data);
        }
      },
      fail: function(res) {
        console.log(res);
        util.go404Page(res.statusCode, res.data);
      },
      complete: function(res) {
        resolve(userProfile);
      }
    })
  })
}

/**
 * 根据用户登录的id获取用户的信息
 */
function getUserProfile(userLoginId) {
  return new Promise(function(resolve, reject) {
    var userProfile = new Object();
    wx.request({
      method: "GET",
      url: baseURLtest + "user_profile/" + userLoginId,
      header: getApp().globalData.header,
      data: {},
      success: function(res) {
        if(res.statusCode == 200) {
          userProfile = res.data;
        }else {
          console.log(res);
          util.go404Page(res.statusCode, res.data);
        }
      },
      fail: function(res) {
        console.log(res);
        util.go404Page(res.statusCode, res.data);
      },
      complete: function(res) {
        resolve(userProfile);
      }
    })
  })
}

/**
 * 关于实习职位的请求
 * 
 * flag: 根据一个行业，根据一个城市
 * internKeyword: 行业字符串，城市字符串
 * 
 */
function listInternshipPosition(internKeyword, flag, pageNow, pageSize) {
  return new Promise(function (resolve, reject) {
    var method = "GET";
    var param = new Object();
    var url = baseURLtest;
    if (flag == "all") {
      url = url + "internship_position?page=" + pageNow + "&&size=" + pageSize;
    } else if (flag == "byTrade") {
      param = internKeyword;
      method = "POST";
      url = url + "internship_position/job_trade?page=" + pageNow + "&&size= " + pageSize;
    } else if (flag == "byCity") {
      url = url + "internship_position/address?address=" + internKeyword + "&&page=" + pageNow + "&&size=" + pageSize;
    }
    var gotInterns = [];
    wx.request({
      method: method,
      url: url, 
      header: getApp().globalData.header,
      data: param,
      success: function (res) {
        if (res.statusCode == 200) {
          gotInterns = res.data;
        } else {
          console.log(res);
          util.go404Page(res.statusCode, res.data);
        }
      },
      fail: function (res) {
        console.log(res);
        util.go404Page(res.statusCode, res.data);
      },
      complete: function (res) {
        resolve(gotInterns);
      }
    })
  })
}

module.exports = {
  getCompanies: getCompanies,
  getCompanyDetailByCompanyId: getCompanyDetailByCompanyId,
  findCompaniesCollection: findCompaniesCollection,
  findCompanyIdsOfCollection: findCompanyIdsOfCollection,
  getHotCompanies: getHotCompanies,
  listPositionsByCompanyName: listPositionsByCompanyName,
  addCollectedStatus: addCollectedStatus,
  getUserProfile: getUserProfile,
  listInternshipPosition: listInternshipPosition,
}