var job_kindData = [
  { index: 1, id: "all1", value: '全部行业', selected: false },
  { index: 2, id: "hr2", value: '互联网/软件', selected: false },
  { index: 3, id: "sd3", value: '设计/动画', selected: false },
  { index: 4, id: "gg4", value: '管培/管理', selected: false },
  { index: 5, id: "xr5", value: '行政/人事', selected: false },
  { index: 6, id: "yg6", value: '营销/公关', selected: false },
  { index: 7, id: "cj7", value: '财税/金融', selected: false },
  { index: 8, id: "wm8", value: '外语/民族语', selected: false },
  { index: 9, id: "fw9", value: '法务', selected: false },
  { index: 10, id: "jj10", value: '教育/机关', selected: false },
  { index: 11, id: "td11", value: '通信/电子', selected: false },
  { index: 12, id: "dt12", value: '地产/土木/建筑', selected: false },
  { index: 13, id: "nd13", value: '能源/电力', selected: false },
  { index: 14, id: "jk14", value: '机械/控制', selected: false },
  { index: 15, id: "wsw15", value: '物流/生产/维修', selected: false },
  { index: 16, id: "hch16", value: '化工/材料/环卫', selected: false },
  { index: 17, id: "kk17", value: '矿产/勘探', selected: false },
  { index: 18, id: "sy18", value: '生化/医药', selected: false },
  { index: 19, id: "nss19", value: '农牧/生态/食品', selected: false },
  { index: 20, id: "xb20", value: '新闻/表演', selected: false },
  { index: 21, id: "swt21", value: '数学/物理/统计', selected: false },
  { index: 22, id: "fz22", value: '服务/职能', selected: false },
];

var citySelectData = [
  { index: 0, id: "bx0", value: '所有城市', selected: false },
  { index: 1, id: "bj1", value: '北京', selected: false },
  { index: 2, id: "sh2", value: '上海', selected: false },
  { index: 3, id: "gz3", value: '广州', selected: false },
  { index: 4, id: "sz4", value: '深圳', selected: false },
  { index: 5, id: "tj5", value: '天津', selected: false },
  { index: 6, id: "wh6", value: '武汉', selected: false },
  { index: 7, id: "cd7", value: '成都', selected: false },
  { index: 8, id: "cq8", value: '重庆', selected: false },
  { index: 9, id: "hz9", value: '杭州', selected: false },
  { index: 10, id: "nj10", value: '南京', selected: false },
  { index: 11, id: "xa11", value: '西安', selected: false },
  { index: 12, id: "sy12", value: '沈阳', selected: false },
  { index: 13, id: "dl13", value: '大连', selected: false },
  { index: 14, id: "cs14", value: '长沙', selected: false },
  { index: 15, id: "nc15", value: '南昌', selected: false },
  { index: 16, id: "qd16", value: '青岛', selected: false },
  { index: 17, id: "jn17", value: '济南', selected: false },
  { index: 18, id: "xm18", value: '厦门', selected: false },
  { index: 19, id: "fz19", value: '福州', selected: false },
  { index: 20, id: "hf20", value: '合肥', selected: false },
  { index: 21, id: "cc21", value: '长春', selected: false },
  { index: 22, id: "sjz22", value: '石家庄', selected: false },
  { index: 23, id: "nn23", value: '南宁', selected: false },
  { index: 24, id: "gy24", value: '贵阳', selected: false },
]

module.exports = {
  job_kindData: job_kindData,
  citySelectData: citySelectData,
}