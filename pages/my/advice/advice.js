// pages/campus/campus.js
var baseURL = getApp().globalData.baseURL;
var baseURLtest = getApp().globalData.baseURLtest;
var util = require("../../../utils/util.js");

Page({
  data: {
    input_value: "",
    content: "",
    phone: "",
    email: ""
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function(options) {
    wx.setNavigationBarTitle({
      title: '建议反馈',
    })
  },

  /**
   * 获取输入信息
   */
  question_des: function(e) {
    this.setData({
      content: e.detail.value
    })
  },
  phone: function(e) {
    this.setData({
      phone: e.detail.value
    })
  },
  email: function(e) {
    this.setData({
      email: e.detail.value
    })
  },

  /**
   * 点击提交按钮
   */
  submit: function(e) {
    var _this = this;
    var param = new Object();
    param.content = this.data.content;
    param.phone = this.data.phone;
    param.email = this.data.email;
    if (param.content == "" || param.phone == "" || param.email == "") {
      wx.showToast({
        title: '请填写完整再提交',
        icon: "none"
      })
      return;
    }
    if(getApp().globalData.userProfile != null){
      param.userProfileId = getApp().globalData.userProfile.id;
    }else{
      param.userProfileId = getApp().globalData.userLogin.id;
    }
    wx.request({
      method: "POST",
      url: baseURLtest + 'proposal',
      header: getApp().globalData.header,
      data: JSON.stringify(param),
      success: function(res) {
        if(res.statusCode == 200) {
          wx.showToast({
            title: '提交成功',
          })
          _this.setData({
            input_value: "",
            content: "",
            phone: "",
            email: ""
          })
        }else{
          console.log(res);
          util.go404Page(res.statusCode, res.data);
        }
      },
      fail: function(res) {
        console.log(res);
        util.go404Page(res.statusCode, res.data);
      }
    })
  }
})