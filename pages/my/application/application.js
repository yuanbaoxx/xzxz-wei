// pages/campus/campus.js
var baseURLtest = getApp().globalData.baseURLtest;
var baseURL = getApp().globalData.baseURL;
var util = require("../../../utils/util.js");

Page({
  data: {
    applications: [],
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    this.listMyApplication();
    wx.setNavigationBarTitle({
      title: '我的投递',
    })
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    this.listMyApplication();
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
    this.listMyApplication();
    wx.stopPullDownRefresh();
  },

  /**
   * 根据用户uid显示我的投递
   */
  listMyApplication: function(e){
    var _this = this;
    if (getApp().globalData.userProfile != null){
      var userProfileId = getApp().globalData.userProfile.id;
      wx.request({
        url: baseURLtest + 'application/' + userProfileId,
        method: "GET",
        header: getApp().globalData.header,
        data: {},
        success: function (res) {
          if (res.statusCode == 200) {
            _this.setData({
              applications: res.data,
            })
          } else {
            console.log(res);
            util.go404Page(res.statusCode, res.data);
          }
        },
        fail: function (res) {
          console.log(res);
          util.go404Page(res.statusCode, res.data);
        },
        complete: function (res) {
          wx.hideNavigationBarLoading();
        }
      })
    }
  }
})