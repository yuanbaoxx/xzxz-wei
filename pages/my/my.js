// pages/campus/campus.js
/**
 * 第一次登陆获得信息保存到缓存，并设置全局变量
 * 每次进入获取缓存，设置全局变量
 * 
 */
var baseURLtest = getApp().globalData.baseURLtest;
var baseURL = getApp().globalData.baseURL;
var util = require("../../utils/util.js");
Page({
  data: {

    //加密数据
    code: "", //用户登录凭证
    encryptedData: "", //bindgetphonenumber按钮得到加密数据
    iv: "", //加密算法的初始向量
    //用户信息
    userLogin: null,
    userProfile: null,
    userName: "",
    userSchool: "",
    //选择登录框的显示
    selectIsShow: true,
    loginIsShow: false,
    wrapIsShow: true,
    canIUse: wx.canIUse('button.open-type.getUserInfo'),

  },

  onLoad: function(options) {
    var _this = this;
    //获取缓存判断是否登录
    wx.getStorage({
      key: 'userLogin',
      success: function(res) {
        console.log("获取缓存的用户登录信息" + res.data);
        getApp().globalData.userLogin = res.data; //设置全局变量
        _this.setData({
          selectIsShow: false,
          wrapIsShow: false,
          userLogin: res.data,
        })
        wx.showToast({
          title: '你已登录',
          icon: "none"
        })
        wx.request({
          method: "GET",
          url: baseURLtest + 'user_profile/' + res.data.id,
          header: getApp().globalData.header,
          data: {},
          success: function(res) {
            if (res.statusCode == 200) {
              if (res.data != "") {
                //用户有简单信息
                var userProfile = res.data;
                getApp().globalData.userProfile = userProfile; //设置全局变量
                _this.setData({
                  userProfile: userProfile,
                  userName: userProfile.name,
                  userSchool: userProfile.school
                })
              } else {
                //用户没有简单信息
                _this.setData({
                  userName: "点击头像完善个人信息",
                  userSchool: ""
                })
              }
            } else {
              console.log(res);
              util.go404Page(res.statusCode, res.data);
            }
          },
          fail: function(res) {
            console.log(res);
            util.go404Page(res.statusCode, res.data);
          }
        })
      },
      fail: function(res) {
        _this.setData({
          selectIsShow: true,
          wrapIsShow: true
        })
      }
    })
    //先获登录凭证code
    wx.login({
      success: function(res) {
        _this.setData({
          code: res.code
        })
      },
      fail: function(res) {}
    })

    wx.setNavigationBarTitle({
      title: '个人中心',
    })
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function() {
    this.getUserInfoDetail();
  },

  /**
   * 下拉刷新
   */
  onPullDownRefresh: function() {
    this.getUserInfoDetail();
    wx.stopPullDownRefresh();
  },

  /**
   * 获取用户详细信息
   */
  getUserInfoDetail: function(event) {
    wx.showNavigationBarLoading();
    var _this = this;
    wx.getStorage({
      key: 'userLogin',
      success: function(res) {
        wx.request({
          method: "GET",
          url: baseURLtest + 'user_profile/' + res.data.id,
          header: getApp().globalData.header,
          data: {},
          success: function(res) {
            if (res.statusCode == 200) {
              if (res.data != "") {
                //用户有简单信息
                var userProfile = res.data;
                getApp().globalData.userProfile = userProfile; //设置全局变量
                _this.setData({
                  userProfile: userProfile,
                  userName: userProfile.name,
                  userSchool: userProfile.school
                })
              } else {
                //用户没有简单信息
                _this.setData({
                  userName: "点击头像完善个人信息",
                  userSchool: ""
                })
              }
            } else {
              console.log(res);
              util.go404Page(res.statusCode, res.data);
            }
          },
          fail: function(res) {
            console.log(res);
            util.go404Page(res.statusCode, res.data);
          }
        })
      },
      fail: function(res) {
        console.log(res);
      },
      complete: function(res) {
        wx.hideNavigationBarLoading();
      }
    })
  },

  /**
   * 选择微信获取手机登录
   */
  getPhoneNumber: function(event) {
    wx.showNavigationBarLoading();
    var _this = this;
    var encryptedData = event.detail.encryptedData;
    var iv = event.detail.iv;
    var code = this.data.code;
    wx.request({
      url: baseURLtest + 'user_login/declassified_phone?code=' + code + "&&iv=" + iv,
      method: "POST",
      header: getApp().globalData.header,
      data: encryptedData,
      success: function(res) {
        if (res.statusCode == 200) {
          var openId = res.data.openId;
          var phoneNumber = res.data.phoneNumber;
          wx.request({
            url: baseURLtest + "user_login/wx_login",
            method: "POST",
            header: getApp().globalData.header,
            data: {
              password: "DEFAULT_WX",
              loginMode: 3,
              openId: openId,
              phone: phoneNumber
            },
            success: function(res) {
              if (res.statusCode == 200) {
                var userLogin = res.data;
                _this.setData({
                  userLogin: userLogin,
                  selectIsShow: false,
                  wrapIsShow: false
                })
                wx.setStorage({
                  key: 'userLogin',
                  data: userLogin,
                })
                getApp().globalData.userLogin = userLogin; //设置全局变量
                wx.showToast({
                  title: '微信登录成功',
                })
                _this.getUserInfoDetail();
              } else {
                console.log(res);
                util.go404Page(res.statusCode, res.data);
              }
            },
            fail: function(res) {
              console.log(res);
              util.go404Page(res.statusCode, res.data);
            },
            complete: function(res) {}
          })
        } else {
          console.log(res);
          util.go404Page(res.statusCode, res.data);
        }
      },
      fail: function(res) {
        console.log(res);
        util.go404Page(res.statusCode, res.data);
      },
      complete: function(res) {
        wx.hideNavigationBarLoading();
      }
    })
  },

  /**
   * 选择手机登录
   */
  goPhoneLogin: function(event) {
    wx.showToast({
      title: '手机登录暂时不可用',
      icon: "none",
      duration: 1000
    })
    /*
    this.setData({
      selectIsShow: false,
      loginIsShow: true
    })*/
  },

  /**
   * 点击返回选择
   */
  backtoSelect: function(event) {
    this.setData({
      selectIsShow: true,
      loginIsShow: false
    })
  },

  /**
   * 用户个人信息
   */
  goUserDetail: function(event) {
    wx.navigateTo({
      url: 'user_detail/user_detail',
    })
  },

  /**
   * 显示我的投递
   */
  goMyApplication: function(e) {
    wx.navigateTo({
      url: 'application/application',
    })
  },

  /**
   * 建议反馈
   */
  advice: function(e) {
    wx.navigateTo({
      url: 'advice/advice',
    })
  },

  /**
   * 点击联系我们
   */
  contactUs: function(res) {
    wx,
    wx.navigateTo({
      url: '/pages/my/contactUs/contactUs',
    })
  },

  /**
   * 点击我的简历
   */
  resume_online_show: function(e) {
    if (this.data.userProfile != null) {
      wx.navigateTo({
        url: '/pages/my/resume/resume_online/resume_online',
      })
    } else {
      wx.showToast({
        title: '点击头像填写简单信息后才可以创建在线简历哦',
        icon: "none",
      })
    }

  },

  /**
   * 点击视频简历进行判断是否上传过视频
   * 有就跳转到video_user 没有跳转到video_example
   */
  go_resume_video: function(e) {
    if (this.data.userProfile != null) {
      var userProfileId = getApp().globalData.userProfile.id;
      wx.request({
        url: baseURLtest + 'video_resume/my_videos?userProfileId=' + userProfileId,
        method: "GET",
        header: getApp().globalData.header,
        success: function(res) {
          if (res.statusCode == 200) {
            if (res.data == null || res.data.length == 0) {
              //没有传递过视频简历
              wx.navigateTo({
                url: '/pages/my/resume/resume_video/video_example/video_example',
              })
            } else {
              //有传递过视频过简历
              wx.navigateTo({
                url: '/pages/my/resume/resume_video/video_user/video_user?userProfileId=' + userProfileId,
              })
            }
          } else {
            console.log(res);
            util.go404Page(res.statusCode, res.data);
          }
        },
        fail: function(res) {
          console.log(res);
          util.go404Page(res.statusCode, res.data);
        }
      })
    } else {
      wx.navigateTo({
        url: '/pages/my/resume/resume_video/video_example/video_example',
      })
    }
  }
})