// pages/my/resume/resume_online/online_modfid/modfid_detail/modfid_detail.js
var baseURLtest = getApp().globalData.baseURLtest;
var baseURL = getApp().globalData.baseURL;
var util = require("../../../../../../utils/util.js");

Page({

  /**
   * 页面的初始数据
   */
  data: {
    resumeId: null,
    id: null,
    idx: null,
    action: null,
    skillQualityId: null,
    notNew: true,

    simple: false,
    base: false,

    start_time: '>',
    end_time: '>',

    //基本信息
    sex_select: '>',
    political: '>',
    birthday: '>',
    city: '>',

    //教育背景
    education: false,
    majorRanking: '>',
    qualification: '>',
    schoolName: "",
    majorCategory: "",

    //实习经历
    internship: false,
    Internship_style: '>',
    companyName: "",
    company_size: '>',
    positionName: "",
    jobDescription: "",

    //项目比赛
    projectContest: false,
    projectName: "",
    Project_scale: '>',
    projectNature: ">",
    ProjectPosition: "",
    projectIntroducation: "",
    projectAchievement: "",

    //校园实践
    schoolExperience: false,
    organizationName: "",
    schoolPositionName: "",
    SchoolWorkDescription: "",

    //荣誉奖励
    honorReward: false,
    worldHonor: "",
    regionHonor: "",
    provinceHonor: "",
    schoolHonor: "",
    othersHonor: "",

    //技能素质
    skillQualitie: false,
    certificate: false,
    certificateName: "",

    //计算机技能
    computerSkill: false,
    computerSkills: null,
    computerSkillName: "",
    computerSkillLevel: ">",
    //语言技能
    languageSkill: false,
    languageSkills: null,
    languageSkillName: "",
    listenTalkLevel: ">",
    readWriteLevel: ">",

    //自我介绍
    selfIntroducationBox: false,
    selfIntroducation: "",

  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function(options) {
    var resumeId = getApp().globalData.onlineResume.id;
    var action = options.action;
    this.setData({
      resumeId: resumeId,
      action: action
    })
    if (action == "addNew") {
      var _type = options._type;
      switch (_type) {
        case 'base':
          this.setData({
            base: true,
            notNew: false,
          })
          break;
        case 'addedu':
          this.setData({
            education: true,
            notNew: false,
          })
          break;
        case 'addinternship':
          this.setData({
            internship: true,
            notNew: false,
          })
          break;
        case 'addproject':
          this.setData({
            projectContest: true,
            notNew: false,
          })
          break;
        case 'addhonorReward':
          this.setData({
            honorReward: true,
            notNew: false,
          })
          break;
        case 'addstuwork':
          this.setData({
            schoolExperience: true,
            notNew: false,
          })
          break;
        case 'addcertificate':
          this.setData({
            certificate: true,
            notNew: false,
          })
          break;
        case 'addselfEvaluation':
          this.setData({
            selfIntroducationBox: true,
            notNew: false,
          })
          break;
      }
    } else if (action = "update") {
      var infoFlag = options.infoFlag;
      this.data.id = options.id;
      this.data.idx = options.idx;
      switch (infoFlag) {
        case 'baseInfo':
          var baseInfo = getApp().globalData.baseInfo
          this.setData({
            base: true,
            political: baseInfo.politics,
            birthday: baseInfo.birthday,
            city: baseInfo.nativePlace,
          })
          break;
        case 'education':
          var educations = getApp().globalData.educations;
          this.setData({
            education: true,
            start_time: educations[this.data.idx].startTime,
            end_time: educations[this.data.idx].endTime,
            majorRanking: educations[this.data.idx].majorRanking,
            qualification: educations[this.data.idx].qualification,
            schoolName: educations[this.data.idx].school,
            majorCategory: educations[this.data.idx].majorCategory,
          })
          break;
        case 'internship':
          var internships = getApp().globalData.internships;
          this.setData({
            internship: true,
            start_time: internships[this.data.idx].startTime,
            end_time: internships[this.data.idx].endTime,
            Internship_style: internships[this.data.idx].internshipType,
            companyName: internships[this.data.idx].companyName,
            company_size: internships[this.data.idx].scale,
            positionName: internships[this.data.idx].positionName,
            jobDescription: internships[this.data.idx].jobDescription,
          })
          break;
        case 'projectContest':
          var projectContests = getApp().globalData.projectContests;
          this.setData({
            projectContest: true,
            start_time: projectContests[this.data.idx].startTime,
            end_time: projectContests[this.data.idx].endTime,
            projectName: projectContests[this.data.idx].projectName,
            Project_scale: projectContests[this.data.idx].scale,
            projectNature: projectContests[this.data.idx].nature,
            ProjectPosition: projectContests[this.data.idx].position,
            projectIntroducation: projectContests[this.data.idx].introducation,
            projectAchievement: projectContests[this.data.idx].achievement,
          })
          break;
        case 'schoolExperience':
          var schoolExperiences = getApp().globalData.schoolExperiences;
          this.setData({
            schoolExperience: true,
            start_time: schoolExperiences[this.data.idx].startTime,
            end_time: schoolExperiences[this.data.idx].endTime,
            organizationName: schoolExperiences[this.data.idx].organizationName,
            schoolPositionName: schoolExperiences[this.data.idx].positionName,
            SchoolWorkDescription: schoolExperiences[this.data.idx].workDescription,
          })
          break;
        case 'honorReward':
          var honorRewards = getApp().globalData.honorRewards;
          this.setData({
            honorReward: true,
            worldHonor: honorRewards[this.data.idx].world,
            regionHonor: honorRewards[this.data.idx].region,
            provinceHonor: honorRewards[this.data.idx].province,
            schoolHonor: honorRewards[this.data.idx].school,
            othersHonor: honorRewards[this.data.idx].others,
          })
          break;
        case 'computerSkill':
          this.setData({
            computerSkill: true,
            computerSkills: getApp().globalData.computerSkills
          })
          break;
        case 'languageSkill':
          this.setData({
            languageSkill: true,
            languageSkills: getApp().globalData.languageSkills
          })
          break;
        case 'certificate':
          var skillQualities = getApp().globalData.skillQualities;
          this.setData({
            certificate: true,
            certificateName: (skillQualities[this.data.idx].certificateName != null) ? skillQualities[this.data.idx].certificateName : "",
          })
          break;
        case 'selfIntroducation':
          var selfIntroducations = getApp().globalData.selfIntroducations
          this.setData({
            selfIntroducationBox: true,
            selfIntroducation: selfIntroducations[this.data.idx].content
          })
          break;
      }
    } else if (action == "simple") {
      this.setData({
        simple: true,
      })
    }
  },

  onHide: function(e) {},

  //开始时间选择
  start_time: function(e) {
    var _this = this;
    _this.setData({
      start_time: e.detail.value,
    })
  },

  //结束时间选择
  end_time: function(e) {
    var _this = this;
    _this.setData({
      end_time: e.detail.value,
    })
  },

  /**********************基本信息模块**************************/
  /**
   *基本信息
    sex_select: '>',
    political: '>',
    birthday: '>',
    city: '>',
   */

  //性别选择
  sex_select: function() {
    var _this = this;
    var sexs = ['男', '女'];
    wx.showActionSheet({
      itemList: ['男', '女'],
      success: function(res) {
        _this.setData({
          sex_select: sexs[res.tapIndex],
        })
      }
    })
  },

  //政治面貌选择
  political: function() {
    var _this = this;
    var politicals = ['共青团员', '中共党员', '群众', '民主党派'];
    wx.showActionSheet({
      itemList: ['共青团员', '中共党员', '群众', '民主党派'],
      success: function(res) {
        _this.setData({
          political: politicals[res.tapIndex],
        })
      }
    })
  },

  //出生年月日选择
  bindDateChange: function(e) {
    var _this = this;
    _this.setData({
      birthday: e.detail.value,
    })
  },

  //籍贯选择
  bindCityChange: function(e) {
    var _this = this;
    _this.setData({
      city: e.detail.value,
    })
  },

  //保存基本信息
  savebaseInfo: function(e) {
    wx.showLoading({
      title: '正在保存',
    })
    var city = this.data.city;
    var cityStr = city[0] + city[1] + city[2];
    var birthday = this.data.birthday;
    var political = this.data.political;
    wx.request({
      url: baseURLtest + "online_resume",
      method: 'POST',
      header: getApp().globalData.header,
      data: {
        id: this.data.resumeId,
        nativePlace: cityStr,
        birthday: birthday,
        politics: political
      },
      success: function(res) {
        if (res.statusCode == 200) {
          wx.hideLoading();
          wx.showToast({
            title: '保存成功',
          })
          wx.navigateBack();
        } else {
          console.log(res);
          util.go404Page(res.statusCode, res.data);
        }
      },
      fail: function(res) {
        console.log(res);
        util.go404Page(res.statusCode, res.data);
      },
      complete: function(res) {
        wx.hideLoading();
      }
    })
  },

  /**********************教育背景模块**************************/
  /**
   *教育背景
    education: false,
    majorRanking: '>',
    qualification: '>',
    schoolName: "",
    majorCategory: "",
   */
  //学历选择
  qualification: function(e) {
    var _this = this;
    var qualifications = ['大专', '本科', '硕士研究生', '博士研究生'];
    wx.showActionSheet({
      itemList: ['大专', '本科', '硕士研究生', '博士研究生'],
      success: function(res) {
        _this.setData({
          qualification: qualifications[res.tapIndex],
        })
      }
    })
  },

  //专业排名选择
  majorRanking: function(e) {
    var _this = this;
    var majorRanking = ['前10%', '前30%', '前50%', '其他'];
    wx.showActionSheet({
      itemList: ['前10%', '前30%', '前50%', '其他'],
      success: function(res) {
        _this.setData({
          majorRanking: majorRanking[res.tapIndex],
        })
      }
    })
  },

  //获取专业信息
  majorCategory: function(e) {
    this.setData({
      majorCategory: e.detail.value,
    })
  },

  //获取就读学校名称
  schoolName: function(e) {
    this.setData({
      schoolName: e.detail.value,
    })
  },

  //保存教育信息
  saveEducation: function(e) {
    wx.showLoading({
      title: '正在保存',
    })
    var param = new Object();
    if (this.data.action == "addNew") {
      param = {
        onlineResumeId: this.data.resumeId,
        school: this.data.schoolName,
        qualification: this.data.qualification,
        majorCategory: this.data.majorCategory,
        startTime: this.data.start_time,
        endTime: this.data.end_time,
        majorRanking: this.data.majorRanking
      }
    } else if (this.data.action == "update") {
      param = {
        id: this.data.id,
        school: this.data.schoolName,
        qualification: this.data.qualification,
        majorCategory: this.data.majorCategory,
        startTime: this.data.start_time,
        endTime: this.data.end_time,
        majorRanking: this.data.majorRanking
      }
    }
    wx.request({
      url: baseURLtest + "education",
      method: 'POST',
      header: getApp().globalData.header,
      data: param,
      success: function(res) {
        if (res.statusCode == 200) {
          wx.hideLoading();
          wx.showToast({
            title: '保存成功',
          })
          wx.navigateBack();
        } else {
          console.log(res);
          util.go404Page(res.statusCode, res.data);
        }
      },
      fail: function(res) {
        console.log(res);
        util.go404Page(res.statusCode, res.data);
      },
      complete: function(res) {
        wx.hideLoading();
      }
    })
  },

  //删除教育信息
  deleteEducation: function(res) {
    var _this = this;
    wx.showModal({
      title: '提示',
      content: '确认删除？',
      success: function(res) {
        wx.showLoading({
          title: '正在删除',
        })
        wx.request({
          url: baseURLtest + "education?id=" + _this.data.id,
          method: 'DELETE',
          header: getApp().globalData.header,
          data: {},
          success: function(res) {
            if (res.statusCode == 200) {
              wx.hideLoading();
              wx.showToast({
                title: '删除成功',
              })
              wx.navigateBack();
            } else {
              console.log(res);
              util.go404Page(res.statusCode, res.data);
            }
          },
          fail: function(res) {
            console.log(res);
            util.go404Page(res.statusCode, res.data);
          },
          complete: function(res) {
            wx.hideLoading();
          }
        })
      },
      fail: function(res) {}
    })
  },

  /**********************实习经历模块**************************/
  /**
   *实习经历
    internship: false,
    Internship_style: '>',
    companyName: "",
    company_size: '>',
    positionName: ">",
    jobDescription: "",
   */
  //实习类型选择
  Internship_style: function(e) {
    var _this = this;
    var Internships = ['全职', '兼职', '实习'];
    wx.showActionSheet({
      itemList: ['全职', '兼职', '实习'],
      success: function(res) {
        _this.setData({
          Internship_style: Internships[res.tapIndex],
        })
      }
    })
  },

  //获取实习公司名称
  companyName: function(e) {
    this.setData({
      companyName: e.detail.value
    })
  },

  //实习公司规模选择
  company_size: function(e) {
    var _this = this;
    var company_sizes = ['50人以内', '50-300人', '300-1000人', '1000-2000人', '2000-3000人', '3000人以上'];
    wx.showActionSheet({
      itemList: ['50人以内', '50-300人', '300-1000人', '1000-2000人', '2000-3000人', '3000人以上'],
      success: function(res) {
        _this.setData({
          company_size: company_sizes[res.tapIndex],
        })
      }
    })
  },

  //获取实习职位名称
  positionName: function(e) {
    this.setData({
      positionName: e.detail.value
    })
  },

  //获取实习工作描述
  jobDescription: function(e) {
    this.setData({
      jobDescription: e.detail.value
    })
  },

  //保存实习经历信息
  saveInternship: function(e) {
    wx.showLoading({
      title: '正在保存',
    })
    var param = new Object();
    if (this.data.action == "addNew") {
      param = {
        onlineResumeId: this.data.resumeId,
        startTime: this.data.start_time,
        endTime: this.data.end_time,
        internshipType: this.data.Internship_style,
        companyName: this.data.companyName,
        scale: this.data.company_size,
        positionName: this.data.positionName,
        jobDescription: this.data.jobDescription,
      }
    } else if (this.data.action == "update") {
      param = {
        id: this.data.id,
        startTime: this.data.start_time,
        endTime: this.data.end_time,
        internshipType: this.data.Internship_style,
        companyName: this.data.companyName,
        scale: this.data.company_size,
        positionName: this.data.positionName,
        jobDescription: this.data.jobDescription,
      }
    }
    wx.request({
      url: baseURLtest + "internship",
      method: 'POST',
      header: getApp().globalData.header,
      data: param,
      success: function(res) {
        if (res.statusCode == 200) {
          wx.hideLoading();
          wx.showToast({
            title: '保存成功',
          })
          wx.navigateBack();
        } else {
          console.log(res);
          util.go404Page(res.statusCode, res.data);
        }
      },
      fail: function(res) {
        console.log(res);
        util.go404Page(res.statusCode, res.data);
      },
      complete: function(res) {
        wx.hideLoading();
      }
    })
  },

  //删除实习信息
  deleteInternship: function(res) {
    var _this = this;
    wx.showModal({
      title: '提示',
      content: '确认删除？',
      success: function(res) {
        wx.showLoading({
          title: '正在删除',
        })
        wx.request({
          url: baseURLtest + "internship?id=" + _this.data.id,
          method: 'DELETE',
          header: getApp().globalData.header,
          data: {},
          success: function(res) {
            if (res.statusCode == 200) {
              wx.hideLoading();
              wx.showToast({
                title: '删除成功',
              })
              wx.navigateBack();
            } else {
              console.log(res);
              util.go404Page(res.statusCode, res.data);
            }
          },
          fail: function(res) {
            console.log(res);
            util.go404Page(res.statusCode, res.data);
          },
          complete: function(res) {
            wx.hideLoading();
          }
        })
      },
      fail: function(res) {}
    })
  },

  /**********************项目比赛模块**************************/
  /**
   *项目比赛
    projectContest: false,
    projectName: "",
    Project_scale: '>',
    projectNature: ">",
    ProjectPosition: "",
    projectIntroducation: "",
    projectAchievement: "",
   */

  //获取项目比赛名称
  projectName: function(e) {
    this.setData({
      projectName: e.detail.value
    })
  },

  //项目比赛规模
  Project_scale: function(e) {
    var _this = this;
    var Project_scales = ['1-3人', '4-10人', '11-20人', '20人以上'];
    wx.showActionSheet({
      itemList: ['1-3人', '4-10人', '11-20人', '20人以上'],
      success: function(res) {
        _this.setData({
          Project_scale: Project_scales[res.tapIndex],
        })
      }
    })
  },

  //项目比赛性质
  projectNature: function(e) {
    var _this = this;
    var projectNature = ['国际级', '国家级', '省级', '校级', '院级', '个人项目'];
    wx.showActionSheet({
      itemList: ['国际级', '国家级', '省级', '校级', '院级', '个人项目'],
      success: function(res) {
        _this.setData({
          projectNature: projectNature[res.tapIndex],
        })
      }
    })
  },

  //获取项目职位
  position: function(e) {
    this.setData({
      ProjectPosition: e.detail.value
    })
  },

  //获取项目简介
  projectIntroducation: function(e) {
    this.setData({
      projectIntroducation: e.detail.value
    })
  },

  //获取项目成就
  projectAchievement: function(e) {
    this.setData({
      projectAchievement: e.detail.value
    })
  },

  //保存项目比赛信息
  saveProjectContest: function(e) {
    wx.showLoading({
      title: '正在保存',
    })
    var param = new Object();
    if (this.data.action == "addNew") {
      param = {
        onlineResumeId: this.data.resumeId,
        startTime: this.data.start_time,
        endTime: this.data.end_time,
        projectName: this.data.projectName,
        scale: this.data.Project_scale,
        nature: this.data.projectNature,
        position: this.data.ProjectPosition,
        introducation: this.data.projectIntroducation,
        achievement: this.data.projectAchievement,
      }
    } else if (this.data.action == "update") {
      param = {
        id: this.data.id,
        startTime: this.data.start_time,
        endTime: this.data.end_time,
        projectName: this.data.projectName,
        scale: this.data.Project_scale,
        nature: this.data.projectNature,
        position: this.data.ProjectPosition,
        introducation: this.data.projectIntroducation,
        achievement: this.data.projectAchievement,
      }
    }
    wx.request({
      url: baseURLtest + "project_contest",
      method: 'POST',
      header: getApp().globalData.header,
      data: param,
      success: function(res) {
        if (res.statusCode == 200) {
          wx.hideLoading();
          wx.showToast({
            title: '保存成功',
          })
          wx.navigateBack();
        } else {
          console.log(res);
          util.go404Page(res.statusCode, res.data);
        }
      },
      fail: function(res) {
        console.log(res);
        util.go404Page(res.statusCode, res.data);
      },
      complete: function(res) {
        wx.hideLoading();
      }
    })
  },

  //删除项目比赛
  deleteProjectContest: function(res) {
    var _this = this;
    wx.showModal({
      title: '提示',
      content: '确认删除？',
      success: function(res) {
        wx.showLoading({
          title: '正在删除',
        })
        wx.request({
          url: baseURLtest + "project_contest?id=" + _this.data.id,
          method: 'DELETE',
          header: getApp().globalData.header,
          data: {},
          success: function(res) {
            if (res.statusCode == 200) {
              wx.hideLoading();
              wx.showToast({
                title: '删除成功',
              })
              wx.navigateBack();
            } else {
              console.log(res);
              util.go404Page(res.statusCode, res.data);
            }
          },
          fail: function(res) {
            console.log(res);
            util.go404Page(res.statusCode, res.data);
          },
          complete: function(res) {
            wx.hideLoading();
          }
        })
      },
      fail: function(res) {}
    })
  },

  /**********************校园实践模块**************************/
  /**
   *校园实践
    schoolExperience: false,
    organizationName: "",
    schoolPositionName: "",
    SchoolWorkDescription: "",
   */

  //获取校园组织名称
  organizationName: function(e) {
    this.setData({
      organizationName: e.detail.value
    })
  },

  //获取职位名称
  schoolPositionName: function(e) {
    this.setData({
      schoolPositionName: e.detail.value
    })
  },

  //获取工作描述
  SchoolWorkDescription: function(e) {
    this.setData({
      SchoolWorkDescription: e.detail.value
    })
  },

  //保存校园实践信息
  saveSchoolExperience: function(e) {
    wx.showLoading({
      title: '正在保存',
    })
    var param = new Object();
    if (this.data.action == "addNew") {
      param = {
        onlineResumeId: this.data.resumeId,
        startTime: this.data.start_time,
        endTime: this.data.end_time,
        organizationName: this.data.organizationName,
        positionName: this.data.schoolPositionName,
        workDescription: this.data.SchoolWorkDescription,
      }
    } else if (this.data.action == "update") {
      param = {
        id: this.data.id,
        startTime: this.data.start_time,
        endTime: this.data.end_time,
        organizationName: this.data.organizationName,
        positionName: this.data.schoolPositionName,
        workDescription: this.data.SchoolWorkDescription,
      }
    }
    wx.request({
      url: baseURLtest + "school_experience",
      method: 'POST',
      header: getApp().globalData.header,
      data: param,
      success: function(res) {
        if (res.statusCode == 200) {
          wx.hideLoading();
          wx.showToast({
            title: '保存成功',
          })
          wx.navigateBack();
        } else {
          console.log(res);
          util.go404Page(res.statusCode, res.data);
        }
      },
      fail: function(res) {
        console.log(res);
        util.go404Page(res.statusCode, res.data);
      },
      complete: function(res) {
        wx.hideLoading();
      }
    })
  },

  //删除校园实践信息
  deleteSchoolExperience: function(res) {
    var _this = this;
    wx.showModal({
      title: '提示',
      content: '确认删除？',
      success: function(res) {
        wx.showLoading({
          title: '正在删除',
        })
        wx.request({
          url: baseURLtest + "school_experience?id=" + _this.data.id,
          method: 'DELETE',
          header: getApp().globalData.header,
          data: {},
          success: function(res) {
            if (res.statusCode == 200) {
              wx.hideLoading();
              wx.showToast({
                title: '删除成功',
              })
              wx.navigateBack();
            } else {
              console.log(res);
              util.go404Page(res.statusCode, res.data);
            }
          },
          fail: function(res) {
            console.log(res);
            util.go404Page(res.statusCode, res.data);
          },
          complete: function(res) {
            wx.hideLoading();
          }
        })
      },
      fail: function(res) {}
    })
  },

  /**********************荣誉奖励模块**************************/
  /**
   *荣誉奖励
    honorReward: false,
    worldHonor: "",
    regionHonor: "",
    provinceHonor: "",
    schoolHonor: "",
    othersHonor: "",
   */

  worldHonor: function(e) {
    this.setData({
      worldHonor: e.detail.value
    })
  },

  regionHonor: function(e) {
    this.setData({
      regionHonor: e.detail.value
    })
  },

  provinceHonor: function(e) {
    this.setData({
      provinceHonor: e.detail.value
    })
  },

  schoolHonor: function(e) {
    this.setData({
      schoolHonor: e.detail.value
    })
  },

  othersHonor: function(e) {
    this.setData({
      othersHonor: e.detail.value
    })
  },

  //保存荣誉奖励信息
  saveHonorReward: function(e) {
    wx.showLoading({
      title: '正在保存',
    })
    var param = new Object();
    if (this.data.action == "addNew") {
      param = {
        onlineResumeId: this.data.resumeId,
        world: this.data.worldHonor,
        region: this.data.regionHonor,
        province: this.data.provinceHonor,
        school: this.data.schoolHonor,
        others: this.data.othersHonor,
      }
    } else if (this.data.action == "update") {
      param = {
        id: this.data.id,
        world: this.data.worldHonor,
        region: this.data.regionHonor,
        province: this.data.provinceHonor,
        school: this.data.schoolHonor,
        others: this.data.othersHonor,
      }
    }
    wx.request({
      url: baseURLtest + "honor_reward",
      method: 'POST',
      header: getApp().globalData.header,
      data: param,
      success: function(res) {
        if (res.statusCode == 200) {
          wx.hideLoading();
          wx.showToast({
            title: '保存成功',
          })
          wx.navigateBack();
        } else {
          console.log(res);
          util.go404Page(res.statusCode, res.data);
        }
      },
      fail: function(res) {
        console.log(res);
        util.go404Page(res.statusCode, res.data);
      },
      complete: function(res) {
        wx.hideLoading();
      }
    })
  },

  //删除荣誉奖励信息
  deleteHonorReward: function(res) {
    var _this = this;
    wx.showModal({
      title: '提示',
      content: '确认删除？',
      success: function(res) {
        wx.showLoading({
          title: '正在删除',
        })
        wx.request({
          url: baseURLtest + "honor_reward?id=" + _this.data.id,
          method: 'DELETE',
          header: getApp().globalData.header,
          data: {},
          success: function(res) {
            if (res.statusCode == 200) {
              wx.hideLoading();
              wx.showToast({
                title: '删除成功',
              })
              wx.navigateBack();
            } else {
              console.log(res);
              util.go404Page(res.statusCode, res.data);
            }
          },
          fail: function(res) {
            console.log(res);
            util.go404Page(res.statusCode, res.data);
          },
          complete: function(res) {
            wx.hideLoading();
          }
        })
      },
      fail: function(res) {}
    })
  },

  /**********************技能素质模块**************************/
  /**
   *技能素质
    skillQualitie: false,
    certificateName: "",
   */
  certificateName: function(e) {
    this.setData({
      certificateName: e.detail.value
    })
  },

  //保存证书
  saveCertificate: function(res) {
    wx.showLoading({
      title: '正在保存',
    })
    var param = new Object();
    if (this.data.action == "addNew") {
      param = {
        onlineResumeId: this.data.resumeId,
        certificateName: this.data.certificateName,
      }
    } else if (this.data.action = "update") {
      param = {
        id: this.data.id,
        certificateName: this.data.certificateName,
      }
    }
    wx.request({
      url: baseURLtest + "skill_quality",
      method: 'POST',
      header: getApp().globalData.header,
      data: param,
      success: function(res) {
        if (res.statusCode == 200) {
          wx.hideLoading();
          wx.showToast({
            title: '保存成功',
          })
          wx.navigateBack();
        } else {
          console.log(res);
          util.go404Page(res.statusCode, res.data);
        }
      },
      fail: function(res) {
        console.log(res);
        util.go404Page(res.statusCode, res.data);
      },
      complete: function(res) {
        wx.hideLoading();
      }
    })
  },

  /**********************技能素质模块**************************/
  /**
   *计算机技能
    computerSkillBox: false,
    computerSkillName: "",
    computerSkillLevel: ">",
    语言技能
    languageSkillBox: false,
    languageSkillName: "",
    listenTalkLevel: ">",
    readWriteLevel: ">",
    证书
    certificateName: "",
   */
  //点击一项语言技能，修改当前技能
  languageSkill: function(e) {
    this.setData({
      id: e.currentTarget.dataset.id,
      idx: e.currentTarget.dataset.idx,
    })
    //点击显示输入框
    var languageSkills = getApp().globalData.languageSkills;
    this.setData({
      languageSkill: false,
      languageSkillBox: true,
      languageSkillName: languageSkills[this.data.idx].languageName,
      listenTalkLevel: languageSkills[this.data.idx].listenTalk,
      readWriteLevel: languageSkills[this.data.idx].readWrite,
    })

  },

  //点击一项计算机技能
  computerSkill: function(e) {
    this.setData({
      id: e.currentTarget.dataset.id,
      idx: e.currentTarget.dataset.idx
    })
    //点击显示输入框
    var computerSkills = getApp().globalData.computerSkills;
    this.setData({
      computerSkill: false,
      computerSkillBox: true,
      computerSkillName: computerSkills[this.data.idx].skillName,
      computerSkillLevel: computerSkills[this.data.idx].skillLevel,
    })
  },

  //计算机技能名字
  computerSkillName: function(e) {
    this.setData({
      computerSkillName: e.detail.value
    })
  },

  //计算机技能水平
  computerSkillLevel: function(e) {
    var _this = this;
    var computerSkillLevel = ['了解', '熟悉', '精通'];
    wx.showActionSheet({
      itemList: ['了解', '熟悉', '精通'],
      success: function(res) {
        _this.setData({
          computerSkillLevel: computerSkillLevel[res.tapIndex],
        })
      }
    })
  },

  //语言技能名称
  languageSkillName: function(e) {
    this.setData({
      languageSkillName: e.detail.value
    })
  },

  //听说水平
  listenTalkLevel: function(e) {
    var _this = this;
    var listenTalkLevel = ['一般', '良好', '精通'];
    wx.showActionSheet({
      itemList: ['一般', '良好', '精通'],
      success: function(res) {
        _this.setData({
          listenTalkLevel: listenTalkLevel[res.tapIndex],
        })
      }
    })
  },

  //读写水平
  readWriteLevel: function(e) {
    var _this = this;
    var readWriteLevel = ['一般', '良好', '精通'];
    wx.showActionSheet({
      itemList: ['一般', '良好', '精通'],
      success: function(res) {
        _this.setData({
          readWriteLevel: readWriteLevel[res.tapIndex],
        })
      }
    })
  },

  //点击新增计算机技能
  addNewComputerSkill: function(e) {
    //点击显示输入框
    this.data.skillQualityId = getApp().globalData.skillQualities[0].id;
    this.setData({
      computerSkill: false,
      computerSkillBox: true,
      notNew: false,
    })
  },

  //保存计算机技能
  saveComputerSkill: function(e) {
    wx.showLoading({
      title: '正在保存',
    })
    var param = new Object();
    if (this.data.skillQualityId == null) {
      param = {
        id: this.data.id,
        skillName: this.data.computerSkillName,
        skillLevel: this.data.computerSkillLevel,
      }
    } else {
      param = {
        skillQualityId: this.data.skillQualityId,
        skillName: this.data.computerSkillName,
        skillLevel: this.data.computerSkillLevel,
      }
    }
    wx.request({
      url: baseURLtest + "computer_skill",
      method: 'POST',
      header: getApp().globalData.header,
      data: param,
      success: function(res) {
        if (res.statusCode == 200) {
          wx.hideLoading();
          wx.showToast({
            title: '保存成功',
          })
          wx.navigateBack();
        } else {
          console.log(res);
          util.go404Page(res.statusCode, res.data);
        }
      },
      fail: function(res) {
        console.log(res);
        util.go404Page(res.statusCode, res.data);
      },
      complete: function(res) {
        wx.hideLoading();
      }
    })
  },

  //删除计算机技能
  deleteComputerSkill: function(res) {
    var _this = this;
    wx.showModal({
      title: '提示',
      content: '确认删除？',
      success: function(res) {
        wx.showLoading({
          title: '正在删除',
        })
        wx.request({
          url: baseURLtest + "computer_skill?id=" + _this.data.id,
          method: 'DELETE',
          header: getApp().globalData.header,
          data: {},
          success: function(res) {
            if (res.statusCode == 200) {
              wx.hideLoading();
              wx.showToast({
                title: '删除成功',
              })
              wx.navigateBack();
            } else {
              console.log(res);
              util.go404Page(res.statusCode, res.data);
            }
          },
          fail: function(res) {
            console.log(res);
            util.go404Page(res.statusCode, res.data);
          },
          complete: function(res) {
            wx.hideLoading();
          }
        })
      },
      fail: function(res) {}
    })
  },

  //点击新增语言技能
  addNewLanguageSkill: function(e) {
    this.setData({
      skillQualityId: getApp().globalData.skillQualities[0].id
    })
    this.setData({
      languageSkill: false,
      languageSkillBox: true,
      notNew: false,
    })
  },

  //保存语言技能
  saveLanguageSkill: function(e) {
    wx.showLoading({
      title: '正在保存',
    })
    var param = new Object();
    if (this.data.skillQualityId == null) {
      param = {
        id: this.data.id,
        languageName: this.data.languageSkillName,
        listenTalk: this.data.listenTalkLevel,
        readWrite: this.data.readWriteLevel,
      }
    } else {
      param = {
        skillQualityId: this.data.skillQualityId,
        languageName: this.data.languageSkillName,
        listenTalk: this.data.listenTalkLevel,
        readWrite: this.data.readWriteLevel,
      }
    }
    wx.request({
      url: baseURLtest + "language_skill",
      method: 'POST',
      header: getApp().globalData.header,
      data: param,
      success: function(res) {
        if (res.statusCode == 200) {
          wx.hideLoading();
          wx.showToast({
            title: '保存成功',
          })
          wx.navigateBack();
        } else {
          console.log(res);
          util.go404Page(res.statusCode, res.data);
        }
      },
      fail: function(res) {
        console.log(res);
        util.go404Page(res.statusCode, res.data);
      },
      complete: function(res) {
        wx.hideLoading();
      }
    })
  },

  //删除语言技能
  deleteLanguageSkill: function(res) {
    var _this = this;
    wx.showModal({
      title: '提示',
      content: '确认删除？',
      success: function(res) {
        wx.showLoading({
          title: '正在删除',
        })
        wx.request({
          url: baseURLtest + "language_skill?id=" + _this.data.id,
          method: 'DELETE',
          header: getApp().globalData.header,
          data: {},
          success: function(res) {
            if (res.statusCode == 200) {
              wx.hideLoading();
              wx.showToast({
                title: '删除成功',
              })
              wx.navigateBack();
            } else {
              console.log(res);
              util.go404Page(res.statusCode, res.data);
            }
          },
          fail: function(res) {
            console.log(res);
            util.go404Page(res.statusCode, res.data);
          },
          complete: function(res) {
            wx.hideLoading();
          }
        })
      },
      fail: function(res) {}
    })
  },

  /**********************自我介绍模块**************************/
  /**
   *自我介绍
    selfIntroducation: false,
    selfIntroducation: "",
   */
  selfIntroducation: function(e) {
    this.setData({
      selfIntroducation: e.detail.value
    })
  },

  //保存自我介绍信息
  saveSelfIntroducation: function(e) {
    wx.showLoading({
      title: '正在保存',
    })
    var param = new Object();
    if (this.data.action == "addNew") {
      param = {
        onlineResumeId: this.data.resumeId,
        content: this.data.selfIntroducation
      }
    } else if (this.data.action == "update") {
      param = {
        id: this.data.id,
        content: this.data.selfIntroducation
      }
    }
    wx.request({
      url: baseURLtest + "self_introducation",
      method: 'POST',
      header: getApp().globalData.header,
      data: param,
      success: function(res) {
        if (res.statusCode == 200) {
          wx.hideLoading();
          wx.showToast({
            title: '保存成功',
          })
          wx.navigateBack();
        } else {
          console.log(res);
          util.go404Page(res.statusCode, res.data);
        }
      },
      fail: function(res) {
        console.log(res);
        util.go404Page(res.statusCode, res.data);
      },
      complete: function(res) {
        wx.hideLoading();
      }
    })
  },
  //删除自我介绍
  deleteSelfIntroducation: function(res) {
    var _this = this;
    wx.showModal({
      title: '提示',
      content: '确认删除？',
      success: function(res) {
        wx.showLoading({
          title: '正在删除',
        })
        wx.request({
          url: baseURLtest + "self_introducation?id=" + _this.data.id,
          method: 'DELETE',
          header: getApp().globalData.header,
          data: {},
          success: function(res) {
            if (res.statusCode == 200) {
              wx.hideLoading();
              wx.showToast({
                title: '删除成功',
              })
              wx.navigateBack();
            } else {
              console.log(res);
              util.go404Page(res.statusCode, res.data);
            }
          },
          fail: function(res) {
            console.log(res);
            util.go404Page(res.statusCode, res.data);
          }
        })
      },
      fail: function(res) {}
    })
  },
})