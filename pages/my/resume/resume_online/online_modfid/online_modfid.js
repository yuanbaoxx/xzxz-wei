// pages/my/resume/resume_online/online_modfid/online_modfid.js
var baseURLtest = getApp().globalData.baseURLtest;
var baseURL = getApp().globalData.baseURL;
var util = require("../../../../../utils/util.js");

function ObjectFormatDate(objectDatas) {
  if (objectDatas != null && objectDatas.length != 0) {
    for (var i = 0; i < objectDatas.length; i++) {
      if (objectDatas[i] != null && objectDatas[i].startTime != null && objectDatas[i].startTime != "") {
        objectDatas[i].startTime = util.formatDate(objectDatas[i].startTime);
      }
      if (objectDatas[i] != null && objectDatas[i].endTime != null && objectDatas[i].endTime != "") {
        objectDatas[i].endTime = util.formatDate(objectDatas[i].endTime);
      }
      if (objectDatas[i] != null && objectDatas[i].birthday != null && objectDatas[i].birthday != "") {
        objectDatas[i].birthday = util.formatDate(objectDatas[i].birthday);
      }
    }
  }
  return objectDatas;
}

Page({

  /**
   * 页面的初始数据
   */
  data: {
    userProfile: null,
    modifiedId: null,
    myOnlineResume: null,
    baseInfo: null,
    computerSkills: null,
    educations: null,
    honorRewards: null,
    internships: null,
    languageSkills: null,
    projectContests: null,
    schoolExperiences: null,
    selfIntroducations: null,
    skillQualities: null,
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function(options) {
    var _this = this;
    _this.setData({
      modifiedId: options.modifiedId,
      userProfile: getApp().globalData.userProfile,
      phone: getApp().globalData.userLogin.phone,
    })
    _this.getOnlineResumeOne();

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function(e) {
    var _this = this;
    _this.getOnlineResumeOne();
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function() {
    this.getOnlineResumeOne();
    wx.stopPullDownRefresh();
  },

  //请求用户这个简历
  getOnlineResumeOne: function(e) {
    var _this = this;
    return new Promise(function(resolve, reject) {
      wx.showLoading({
        title: '',
      })
      wx.request({
        url: baseURLtest + "online_resume/" + _this.data.modifiedId,
        method: "GET",
        header: getApp().globalData.header,
        data: {},
        success: function(res) {
          if (res.statusCode == 200) {
            var myOnlineResume = res.data;
            var baseInfos = myOnlineResume.baseInfo;
            getApp().globalData.onlineResume = baseInfos[0];
            getApp().globalData.baseInfo = ObjectFormatDate(myOnlineResume.baseInfo),
              getApp().globalData.computerSkills = ObjectFormatDate(myOnlineResume.computerSkills),
              getApp().globalData.educations = ObjectFormatDate(myOnlineResume.educations),
              getApp().globalData.honorRewards = ObjectFormatDate(myOnlineResume.honorRewards),
              getApp().globalData.internships = ObjectFormatDate(myOnlineResume.internships),
              getApp().globalData.languageSkills = ObjectFormatDate(myOnlineResume.languageSkills),
              getApp().globalData.projectContests = ObjectFormatDate(myOnlineResume.projectContests),
              getApp().globalData.schoolExperiences = ObjectFormatDate(myOnlineResume.schoolExperiences),
              getApp().globalData.selfIntroducations = ObjectFormatDate(myOnlineResume.selfIntroducations),
              getApp().globalData.skillQualities = ObjectFormatDate(myOnlineResume.skillQualities),
              _this.setData({
                myOnlineResume: myOnlineResume,
                baseInfo: ObjectFormatDate(myOnlineResume.baseInfo),
                computerSkills: ObjectFormatDate(myOnlineResume.computerSkills),
                educations: ObjectFormatDate(myOnlineResume.educations),
                honorRewards: ObjectFormatDate(myOnlineResume.honorRewards),
                internships: ObjectFormatDate(myOnlineResume.internships),
                languageSkills: ObjectFormatDate(myOnlineResume.languageSkills),
                projectContests: ObjectFormatDate(myOnlineResume.projectContests),
                schoolExperiences: ObjectFormatDate(myOnlineResume.schoolExperiences),
                selfIntroducations: ObjectFormatDate(myOnlineResume.selfIntroducations),
                skillQualities: ObjectFormatDate(myOnlineResume.skillQualities),
              })
            wx.hideNavigationBarLoading();
          } else {
            console.log(res);
            util.go404Page(res.statusCode, res.data);
          }
        },
        fail: function(res) {
          console.log(res);
          util.go404Page(res.statusCode, res.data);
        },
        complete: function(res) {
          wx.hideLoading();
        }
      })
    })
  },

  /**
   * 点击个人素质的增加按钮
   */
  addSkillQuality: function(e) {
    this.addNewSkillQuality();
  },

  /**
   * 先增加一条技能记录
   */
  addNewSkillQuality: function (res) {
    var _this = this;
    return new Promise(function (resolve, reject) {
      wx.request({
        url: baseURLtest + "skill_quality",
        method: "POST",
        header: getApp().globalData.header,
        data: {
          onlineResumeId: getApp().globalData.onlineResume.id,
        },
        success: function (res) {
          if (res.statusCode == 200) {
            var arr = [];
            arr.push(res.data);
            _this.setData({
              skillQualities: arr,
              computerSkills: [],
              languageSkills: [],
            })
            getApp().globalData.skillQualities = arr;
            getApp().globalData.computerSkills = [];
            getApp().globalData.languageSkills = [];
          } else {
            console.log(res);
            util.go404Page(res.statusCode, res.data);
          }
        },
        fail: function (res) {
          console.log(res);
          util.go404Page(res.statusCode, res.data);
        },
        complete: function (res) {
          resolve();
        }
      })
    })
  },

  /**
   * 点击增加按钮
   */
  addNew: function(e) {
    var _type = e.currentTarget.dataset.type;
    wx.navigateTo({
      url: "/pages/my/resume/resume_online/online_modfid/modfid_detail/modfid_detail?action=addNew&&_type=" + _type,
    })
  },

  /**
   * 点击修改当前这一条信息
   */
  updateThisInfo: function(e) {
    /**
     * 把标识信息传过来，根据标识信息，显示页面，获取数据，保存的地址，
     * 标识信息和此条信息的id
     */
    var infoFlag = e.currentTarget.dataset.infoFlag;
    var id = e.currentTarget.dataset.id;
    var idx = e.currentTarget.dataset.idx;
    wx.navigateTo({
      url: "/pages/my/resume/resume_online/online_modfid/modfid_detail/modfid_detail?action=update&&infoFlag=" + infoFlag + "&&id=" + id + "&&idx=" + idx
    })
  },

  //点击修改简单信息
  modifiedSimple: function(res) {
    wx.navigateTo({
      url: "/pages/my/user_detail/user_detail?action=fromOnlineResume",
    })
  },
})