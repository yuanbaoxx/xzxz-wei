// pages/my/resume/resume_online/resume_online.js
var baseURL = getApp().globalData.baseURL;
var baseURLtest = getApp().globalData.baseURLtest;
var util = require("../../../../utils/util.js");

function ObjectFormatDate(objectDatas) {
  if (objectDatas != null && objectDatas.length != 0) {
    for (var i = 0; i < objectDatas.length; i++) {
      if (objectDatas[i] != null && objectDatas[i].startTime != null && objectDatas[i].startTime != "") {
        objectDatas[i].startTime = util.formatDate(objectDatas[i].startTime);
      }
      if (objectDatas[i] != null && objectDatas[i].endTime != null && objectDatas[i].endTime != "") {
        objectDatas[i].endTime = util.formatDate(objectDatas[i].endTime);
      }
      if (objectDatas[i] != null && objectDatas[i].birthday != null && objectDatas[i].birthday != "") {
        objectDatas[i].birthday = util.formatDate(objectDatas[i].birthday);
      }
    }
  }
  return objectDatas;
}

Page({

  /**
   * 页面的初始数据
   */
  data: {
    modified: false,
    black: false,
    myOnlineResumes: [],
    modifiedId: 0,
    newOnlineResumeName: null,
    maxOnlineResume: 3,
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function(options) {
    wx.showLoading({
      title: '',
    })
    this.getsimpleOnlineResumeList();
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function() {
    this.getsimpleOnlineResumeList();
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function() {
    this.getsimpleOnlineResumeList();
    wx.stopPullDownRefresh();
  },

  //获取用户的所有简历信息
  getsimpleOnlineResumeList: function(e) {
    wx.showNavigationBarLoading();
    var _this = this;
    var userProfileId = getApp().globalData.userProfile.id
    wx.request({
      url: baseURLtest + "online_resume/myOnlineResumes?userProfileId=" + userProfileId,
      header: getApp().globalData.header,
      method: "GET",
      data: {},
      success: function(res) {
        if (res.statusCode == 200) {
          var onlineResumes = res.data;
          var currentId = null;
          if (onlineResumes != null && onlineResumes.length != 0) {
            currentId = onlineResumes[0].id;
          }
          //遍历onlineResumes里面的onlineResumeId,请求这个简历的所有信息
          _this.getOneResumeDetail(onlineResumes.length, 0, onlineResumes).then((_res) => {
            _this.setData({
              modifiedId: currentId,
              myOnlineResumes: _res,
            })
            wx.hideLoading();
          });
        } else {
          console.log(res);
          util.go404Page(res.statusCode, res.data);
        }
      },
      fail: function(res) {
        console.log(res);
        util.go404Page(res.statusCode, res.data);
      },
      complete: function(res) {
        wx.hideNavigationBarLoading();
      }
    })
  },

  //获取所有简历详细信息
  getOneResumeDetail: function(max, count, onlineResumes) {
    var _this = this;
    return new Promise(function(resolve, reject) {
      if (count < max) {
        wx.request({
          url: baseURLtest + "online_resume/" + onlineResumes[count].id,
          method: "GET",
          header: getApp().globalData.header,
          data: {},
          success: function(res) {
            if (res.statusCode == 200) {
              var resumeDetail = res.data;
              resumeDetail.baseInfo = ObjectFormatDate(resumeDetail.baseInfo);
              resumeDetail.educations = ObjectFormatDate(resumeDetail.educations);
              resumeDetail.internships = ObjectFormatDate(resumeDetail.internships);
              resumeDetail.projectContests = ObjectFormatDate(resumeDetail.projectContests);
              resumeDetail.schoolExperiences = ObjectFormatDate(resumeDetail.schoolExperiences);
              resumeDetail.userProfile = getApp().globalData.userProfile;
              resumeDetail.userLogin = getApp().globalData.userLogin;
              onlineResumes[count].resumeDetail = resumeDetail;
              count++;
              _this.getOneResumeDetail(max, count, onlineResumes).then((_res) => {
                resolve(_res);
              })
            } else {
              console.log(res);
              util.go404Page(res.statusCode, res.data);
            }
          },
          fail: function(res) {
            console.log(res);
            util.go404Page(res.statusCode, res.data);
          }
        })
      } else {
        resolve(onlineResumes);
      }
    })
  },


  //swiper滑动
  bindchange: function(e) {
    var idx = e.detail.current;
    var myOnlineResumes = this.data.myOnlineResumes;
    this.setData({
      modifiedId: myOnlineResumes[idx].id,
    })
  },


  //点击简历编辑选择框
  choose_resume: function(e) {
    this.setData({
      modified: true,
      black: true,
    })
  },

  //放弃编辑
  quit_resume: function(e) {
    this.setData({
      modified: false,
      reNameBox: false,
      black: false
    })
  },

  //编辑当前简历
  modifiedThisOnlineResume: function(res) {
    this.setData({
      modified: false,
      black: false,
    })
    wx.navigateTo({
      url: "/pages/my/resume/resume_online/online_modfid/online_modfid?modifiedId=" + this.data.modifiedId,
    })
  },

  //点击制作新简历
  new_resume: function(e) {
    //先增加一条空简历，再跳转到编辑页面，请求这个空简历
    if (getApp().globalData.userProfile != null) {
      if (this.data.myOnlineResumes.length >= this.data.maxOnlineResume) {
        wx.showToast({
          title: '最多只能保存三个简历，不能贪心哦',
          icon: "none",
          duration: 2000
        })
      } else {
        var userProfileId = getApp().globalData.userProfile.id;
        wx.request({
          url: baseURLtest + "online_resume",
          method: "POST",
          header: getApp().globalData.header,
          data: {
            userProfileId: userProfileId,
            resumeName: "我的新简历",
          },
          success: function(res) {
            if (res.statusCode == 200) {
              wx.navigateTo({
                url: "/pages/my/resume/resume_online/online_modfid/online_modfid?modifiedId=" + res.data.id,
              })
            } else {
              console.log(res);
              util.go404Page(res.statusCode, res.data);
            }
          },
          fail: function(res) {
            console.log(res);
            util.go404Page(res.statusCode, res.data);
          }
        })
      }
    } else {
      wx.showModal({
        title: '提示',
        content: '先去个人中心填写简单信息',
        success: function(res) {
          if (res.confirm) {
            //用户点击确认
            wx.switchTab({
              url: '/pages/my/user_detail/detail',
            })
          } else if (res.cancel) {
            //用户点击取消
          }
        }
      })
    }
  },

  //点击设为默认
  setDeafult: function(e) {
    getApp().globalData.defaultOnlineResume = this.data.modifiedId;
    this.setData({
      modified: false,
      black: false
    })
    wx.showToast({
      title: '设置成功',
    })
  },

  //重新命名此视频名称
  reName: function(res) {
    this.setData({
      modified: false,
      reNameBox: true,
      black: true,
    })
  },

  //輸入名字
  inputName: function(e) {
    this.setData({
      newOnlineResumeName: e.detail.value
    })
  },

  //重命名保存
  reNameConfirm: function(e) {
    var _this = this;
    var modifiedId = _this.data.modifiedId;
    var newOnlineResumeName = _this.data.newOnlineResumeName;
    var param = {
      "id": modifiedId,
      "resumeName": newOnlineResumeName
    }
    this.setData({
      reNameBox: false,
      black: false
    })
    wx.showLoading({
      title: '正在更新名字',
    })
    wx.request({
      url: baseURLtest + '/online_resume',
      method: "POST",
      header: getApp().globalData.header,
      data: param,
      success: function(res) {
        if (res.statusCode == 200) {
          var myOnlineResumes = _this.data.myOnlineResumes;
          for (var i = 0; i < myOnlineResumes.length; i++) {
            if (res.data.id == myOnlineResumes[i].id) {
              myOnlineResumes[i].resumeName = res.data.resumeName;
            }
          }
          _this.setData({
            myOnlineResumes: myOnlineResumes
          })
          wx.hideLoading();
          wx.showToast({
            title: '更新成功',
          })
        } else {
          console.log(res);
          util.go404Page(res.statusCode, res.data);
        }
      },
      fail: function(res) {
        wx.hideLoading();
        wx.showToast({
          title: '更新名字失败，请重试',
          icon: "none"
        })
      },
      complete: function(res) {}
    })
  },

  //取消更新名字
  reNameCancle: function(res) {
    this.setData({
      reNameBox: false,
      black: false
    })
  },

  //删除此视频记录
  deleteThisResume: function(e) {
    var _this = this;
    wx.showModal({
      title: '提示框',
      content: '确认删除此简历？',
      success: function(res) {
        if (res.confirm) {
          _this.setData({
            modified: false,
            black: false,
          })
          wx.showNavigationBarLoading();
          var modifiedId = _this.data.modifiedId;
          wx.request({
            url: baseURLtest + 'online_resume/' + modifiedId,
            method: "DELETE",
            header: getApp().globalData.header,
            data: {},
            success: function(res) {
              if (res.statusCode == 200) {
                wx.showToast({
                  title: '删除成功',
                })
                var myOnlineResumes = _this.data.myOnlineResumes;
                for (var i = 0; i < myOnlineResumes.length; i++) {
                  if (modifiedId == myOnlineResumes[i].id) {
                    myOnlineResumes.splice(i, 1);
                  }
                }
                _this.setData({
                  myOnlineResumes: myOnlineResumes
                })
              } else {
                console.log(res);
                util.go404Page(res.statusCode, res.data);
              }
            },
            fail: function(res) {
              console.log(res);
              util.go404Page(res.statusCode, res.data);
            },
            complete: function(res) {
              wx.hideNavigationBarLoading();
            }
          })
        } else if (res.cancel) {}
      },
      fail: function(res) {
        _this.setData({
          modified: false,
          black: false,
        })
      }
    })
  },
})