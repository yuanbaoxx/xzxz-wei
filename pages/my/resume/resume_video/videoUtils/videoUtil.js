/**
 * 将视频记录加上flag和isPlay属性
 * flag: 1视频，0图片；
 * isPlay: 视频是否播放
 * 
 */
var baseURL = getApp().globalData.baseURL;
var baseURLtest = getApp().globalData.baseURLtest;
var util = require("../../../../../utils/util.js");

function addVideoAttr(videoArray) {
  return new Promise(function(resolve, reject) {
    for (var i = 0; i < videoArray.length; i++) {
      videoArray[i].flag = 0;
      videoArray[i].isPlay = false;
    }
    resolve(videoArray);
  })
}

/**
 *根据用户id请求用户的视频简历信息 
 */
function getResumeVideoByUserProfileId(userProfileId) {
  return new Promise(function(resolve, reject) {
    var myResumeVideos = [];
    wx.request({
      url: baseURLtest + 'video_resume/my_videos?userProfileId=' + userProfileId,
      method: "GET",
      data: {},
      header: getApp().globalData.header,
      success: function(res) {
        if (res.statusCode == 200) {
          myResumeVideos = res.data;
        } else {
          console.log(res);
          util.go404Page(res.statusCode, res.data);
        }
      },
      fail: function(res) {
        console.log(res);
        util.go404Page(res.statusCode, res.data);
      },
      complete: function(res) {
        resolve(myResumeVideos);
      }
    })
  })
}

module.exports = {
  addVideoAttr: addVideoAttr,
  getResumeVideoByUserProfileId: getResumeVideoByUserProfileId
}