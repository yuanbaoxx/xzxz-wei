var videoData = require("../videoUtils/videoData.js").videoData;
var baseURLtest = getApp().globalData.baseURLtest;
var util = require("../../../../../utils/util.js");

Page({

  /**
   * 页面的初始数据
   */
  data: {
    tab_item: 'block',
    videoData: videoData,
    maxVideo: 2,
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function(options) {
    this.setData({
      videoData: videoData
    })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function(e) {
    this.videos = [];
    //this.videos=[];
    var _videoDate = this.data.videoData;
    for (var i = 0; i < _videoDate.length; i++) {
      this.videos.push(wx.createVideoContext(_videoDate[i].id, this));
    }

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function() {},

  //点击播放按钮，封面图片隐藏,播放视频
  videoPlay: function(e) {
    var vid = e.currentTarget.dataset.id; //图片点击事件传进来的视频id
    var videoData = this.data.videoData; //前端的视频数据
    for (var j = 0; j < videoData.length; j++) {
      if (videoData[j].id == vid) { //找出点击的是哪个视频的封面
        videoData[j].isPlay = true; //设置成播放状态
        this.videos[j].play(); //播放
        break;
      }
    }
    this.setData({
      videoData: videoData
    })

  },

  //轮播图滚动时图片显示
  change_img_url: function(e) {
    //获取页面上的所有视频，循环判断哪个视频在播放，然后让视频停止播放
    for (var j = 0; j < this.videos.length; j++) {
      this.videos[j].pause();
    }
  },

  bindwaiting: function(e) {
    console.log(e);
  },

  /**
   * 用户点击增加简历
   */
  addNewVideo: function(e) {
    var _this = this;
    if (getApp().globalData.userProfile != null) {
      if (getApp().globalData.resumeVideos.length >= this.data.maxVideo) {
        wx.showToast({
          title: '最多只能上传两个视频简历，不能贪心哦',
          icon: "none",
          duration: 2000
        })
      } else {
        var param = {
          userProfileId: getApp().globalData.userProfile.id,
          videoName: getApp().globalData.userProfile.name + "的视频简历",
          description: getApp().globalData.userProfile.name + "的视频简历",
          reviewed: "false"
        };
        wx.chooseVideo({
          sourceType: ['album', 'camera'],
          maxDuration: 60,
          camera: 'back',
          success: function(res) {
            wx.showLoading({
              title: '正在上传中',
            })
            var tempVideoData = res;
            wx.uploadFile({
              url: baseURLtest + 'video_resume/update_video',
              filePath: tempVideoData.tempFilePath,
              name: 'resume_video',
              formData: param,
              success: function(res) {
                if (res.statusCode == 200) {
                  wx.hideLoading();
                  wx.showToast({
                    title: '上传视频成功',
                  })
                  wx.redirectTo({
                    url: '/pages/my/resume/resume_video/video_user/video_user',
                  })
                } else {
                  console.log(res);
                  util.go404Page(res.statusCode, res.data);
                }
              },
              fail: function(res) {
                console.log(res);
                util.go404Page(res.statusCode, res.data);
              },
              complete: function(res) {
                wx.hideLoading();
              }
            })
          },
          fail: function(res) {},
          complete: function(res) {}
        })
      }
    } else {
      wx.showModal({
        title: '提示',
        content: '先去个人中心填写简单信息',
        success: function(res) {
          if (res.confirm) {
            //用户点击确认
            wx.switchTab({
              url: '/pages/my/user_detail/detail',
            })
          } else if (res.cancel) {
            //用户点击取消
          }
        }
      })
    }
  },

})