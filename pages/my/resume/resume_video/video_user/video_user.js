// pages/my/resume/resume_video/video_user/video_user.js
var videoUtil = require("../videoUtils/videoUtil.js");
var baseURL = getApp().globalData.baseURL;
var baseURLtest = getApp().globalData.baseURLtest;
var util = require("../../../../../utils/util.js");

Page({

  /**
   * 页面的初始数据
   */
  data: {
    tab_item: 'block',
    videoData: [],
    modified: false,
    reNameBox: false,
    black: false,
    maxVideo: 2,
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function(options) {
    wx.showLoading({
      title: '',
    })
    var _this = this;
    var userProfileId = getApp().globalData.userProfile.id;
    videoUtil.getResumeVideoByUserProfileId(userProfileId).then((res) => {
      var myResumeVideos = [];
      var currentItemId = -1;
      if (res != null && res.length != 0) {
        myResumeVideos = res;
        getApp().globalData.resumeVideos = res;
        currentItemId = myResumeVideos[0].id;
      }
      _this.setData({
        videoData: myResumeVideos,
        currentItemId: currentItemId
      })
      this.videos = [];
      var _videoDate = this.data.videoData;
      for (var i = 0; i < _videoDate.length; i++) {
        this.videos.push(wx.createVideoContext(_videoDate[i].id + "", this));
      }
      wx.hideLoading();
    })
  },

  /**
   * 监听页面显示
   */
  onShow: function(res) {
    wx.showLoading({
      title: '',
    })
    var _this = this;
    var userProfileId = getApp().globalData.userProfile.id;
    videoUtil.getResumeVideoByUserProfileId(userProfileId).then((res) => {
      var myResumeVideos = [];
      var currentItemId = -1;
      if (res != null && res.length != 0) {
        myResumeVideos = res;
        getApp().globalData.resumeVideos = res;
        currentItemId = myResumeVideos[0].id;
      }
      _this.setData({
        videoData: myResumeVideos,
        currentItemId: currentItemId
      })
      this.videos = [];
      var _videoDate = this.data.videoData;
      for (var i = 0; i < _videoDate.length; i++) {
        this.videos.push(wx.createVideoContext(_videoDate[i].id + "", this));
      }
      wx.hideLoading();
    })

  },

  /**
   * 监听页面隐藏
   */
  onHide: function(e) {
    for (var j = 0; j < this.videos.length; j++) {
      this.videos[j].pause();
    }
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function() {
    this.setData({
      videoData: [],
    })
    wx.showLoading({
      title: '',
    })
    var _this = this;
    var userProfileId = getApp().globalData.userProfile.id;
    videoUtil.getResumeVideoByUserProfileId(userProfileId).then((res) => {
      var myResumeVideos = [];
      var currentItemId = -1;
      if (res != null && res.length != 0) {
        myResumeVideos = res;
        currentItemId = myResumeVideos[0].id;
      }
      _this.setData({
        videoData: myResumeVideos,
        currentItemId: currentItemId
      })
      this.videos = [];
      var _videoDate = this.data.videoData;
      for (var i = 0; i < _videoDate.length; i++) {
        this.videos.push(wx.createVideoContext(_videoDate[i].id + "", this));
      }
      wx.hideLoading();
      wx.stopPullDownRefresh();
    })
  },

  //点击播放按钮，封面图片隐藏,播放视频
  videoPlay: function(e) {
    var vid = e.currentTarget.dataset.id; //图片点击事件传进来的视频id
    var videoData = this.data.videoData; //前端的视频数据
    for (var j = 0; j < videoData.length; j++) {
      if (videoData[j].id == vid) { //找出点击的是哪个视频的封面
        videoData[j].isPlay = true; //设置成播放状态
        this.videos[j].play(); //播放
        break;
      }
    }
    this.setData({
      videoData: videoData
    })

  },

  //轮播图滚动时图片显示
  change_img_url: function(e) {
    //获取页面上的所有视频，循环判断哪个视频在播放，然后让视频停止播放
    for (var j = 0; j < this.videos.length; j++) {
      this.videos[j].pause();
    }
  },

  camera_camera: function(e) {
    var _this = this;
    wx.chooseVideo({
      sourceType: ['album', 'camera'],
      maxDuration: 60,
      camera: 'back',
      success(res) {
        var videoData = _this.data.videoData;
        videoData[0].url = 'wxfile://tmp_e26e759dc00fabcd6352eabe86b5427756f68ba70b48d6f5.mp4';
        _this.setData({
          videoData: videoData
        })
      }
    })
  },

  //点击查看示例
  videoExample: function(e) {
    wx.navigateTo({
      url: "/pages/my/resume/resume_video/video_example/video_example",
    })
  },

  //点击编辑简历
  modify_video: function(e) {
    this.setData({
      modified: true,
      black: true
    })
  },

  //退出编辑
  quit_modified: function(e) {
    this.setData({
      modified: false,
      black: false
    })
  },

  currentChange: function(e) {
    this.setData({
      currentItemId: e.detail.currentItemId
    })

  },

  //用户上传新视频
  addNewVideo: function(res) {
    if (this.data.videoData.length >= this.data.maxVideo) {
      wx.showToast({
        title: '最多只能上传两个视频简历，不能贪心哦',
        icon: "none",
        duration: 2000
      })
    } else {
      this.setData({
        modified: false,
        black: false
      })
      var _this = this;
      var param = {
        userProfileId: getApp().globalData.userProfile.id,
        videoName: getApp().globalData.userProfile.name + "的视频简历",
        description: getApp().globalData.userProfile.name + "的视频简历",
        reviewed: "false"
      };
      wx.chooseVideo({
        sourceType: ['album', 'camera'],
        maxDuration: 60,
        camera: 'back',
        success: function(res) {
          wx.showLoading({
            title: '正在上传中',
          })
          var tempVideoData = res;
          wx.uploadFile({
            url: baseURLtest + 'video_resume/update_video',
            filePath: tempVideoData.tempFilePath,
            name: 'resume_video',
            formData: param,
            success: function(res) {
              if (res.statusCode == 200) {
                wx.hideLoading();
                var videoFromDataBase = resData.videoResume;
                var myResumeVideos = _this.data.videoData;
                myResumeVideos.push(videoFromDataBase);
                _this.setData({
                  videoData: myResumeVideos
                })
                wx.showToast({
                  title: '上传视频成功',
                })
              } else {
                console.log(res);
                util.go404Page(res.statusCode, res.data);
              }
            },
            fail: function(res) {
              console.log(res);
              util.go404Page(res.statusCode, res.data);
            },
            complete: function(res) {
              wx.hideLoading();
            }
          })
        },
        fail: function(res) {
          wx.showToast({
            title: '选取视频失败' + res,
            icon: "none"
          })
        }
      })
    }
  },

  //用户重新上传此条记录的视频
  reLoadMyVideo: function(res) {},

  //将此视频记录设置为默认
  setVideoDefault: function(res) {
    getApp().globalData.defaultVideoResume = this.data.currentItemId;
    this.setData({
      modified: false,
      black: false
    })
    wx.showToast({
      title: '设置成功',
    })
  },

  //重新命名此视频名称
  reNameMyVideo: function(res) {
    this.setData({
      modified: false,
      reNameBox: true,
      black: true,
    })
  },

  inputName: function(e) {
    this.setData({
      newVideoName: e.detail.value
    })
  },

  //重命名确认
  reNameConfirm: function(e) {
    var _this = this;
    var currentItemId = _this.data.currentItemId;
    var newVideoName = _this.data.newVideoName;
    var param = {
      "id": currentItemId,
      "videoName": newVideoName
    }
    this.setData({
      reNameBox: false,
      black: false
    })
    wx.showLoading({
      title: '正在更新名字',
    })
    wx.request({
      url: baseURLtest + 'video_resume/update_info',
      method: "POST",
      header: getApp().globalData.header,
      data: param,
      success: function(res) {
        var videoData = _this.data.videoData;
        for (var i = 0; i < videoData.length; i++) {
          if (res.data.id == videoData[i].id) {
            videoData[i].videoName = res.data.videoName;
          }
        }
        _this.setData({
          videoData: videoData
        })
        wx.hideLoading();
        wx.showToast({
          title: '更新成功',
        })
      },
      fail: function(res) {
        wx.hideLoading();
        wx.showToast({
          title: '更新名字失败，请重试',
          icon: "none"
        })
      }
    })
  },

  //取消更新名字
  reNameCancle: function(res) {
    this.setData({
      reNameBox: false,
      black: false
    })
  },

  //删除此视频记录
  deletemyVideo: function(res) {
    var _this = this;
    wx.showModal({
      title: '提示框',
      content: '确认删除此简历视频？',
      success: function(res) {
        wx.showNavigationBarLoading();
        var currentItemId = _this.data.currentItemId;
        wx.request({
          url: baseURLtest + 'video_resume/' + currentItemId,
          method: "DELETE",
          header: getApp().globalData.header,
          data: {},
          success: function(res) {
            if (res.statusCode == 200) {
              wx.showToast({
                title: '删除成功',
              })
              var videoData = _this.data.videoData;
              for (var i = 0; i < videoData.length; i++) {
                if (currentItemId == videoData[i].id) {
                  videoData.splice(i, 1);
                }
              }
              _this.setData({
                videoData: videoData
              })
              if(videoData.length == 0) {
                wx.redirectTo({
                  url: '/pages/my/resume/resume_video/video_example/video_example'
                })
              }
            } else {
              console.log(res);
              util.go404Page(res.statusCode, res.data);
            }
          },
          fail: function(res) {
            console.log(res);
            util.go404Page(res.statusCode, res.data);
          },
          complete: function(res) {
            wx.hideNavigationBarLoading();
          }
        })
      },
      complete: function(res) {
        _this.setData({
          modified: false,
          black: false
        })
      }
    })
  },

})