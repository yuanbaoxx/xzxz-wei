// pages/my/user_detail/user_detail.js
//全局参数是否有用户信息，没有则填写，有则显示，填写后，重新调用请求信息的函数，设置数据，渲染页面
var util = require('../../../utils/util.js'); //公共函数
var baseURL = getApp().globalData.baseURL;
var baseURLtest = getApp().globalData.baseURLtest;

Page({
  data: {
    name: "",
    gender: "",
    edu: "",
    school: "",
    major: "",
    graduateYear: "",
    email: "",
    ExitOfTheUserinfo: true,
    userProfile: null,
    phone: "",
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function(options) {
    wx.setNavigationBarTitle({
      title: '关于我',
    })
    this.setData({
      phone: getApp().globalData.userLogin.phone
    })
    //全局变量是否有用户信息
    if (getApp().globalData.userProfile == null) {
      //没有信息
      this.setData({
        ExitOfTheUserinfo: false
      })
    } else {
      //有信息
      this.setData({
        userProfile: getApp().globalData.userProfile,
        ExitOfTheUserinfo: true
      })
    }
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function() {
    this.getUserInfoDetail().then(() => {
      wx.stopPullDownRefresh();
    });
  },

  /**
   * 根据用户登录信息主键请求用户简单信息
   */
  getUserInfoDetail: function(event) {
    wx.showNavigationBarLoading();
    var _this = this;
    return new Promise(function(resolve, reject) {
      var userLoginId = getApp().globalData.userLogin.id;
      wx.request({
        method: "GET",
        url: baseURLtest + 'user_profile/' + userLoginId,
        header: getApp().globalData.header,
        data: {},
        success: function(res) {
          if (res.statusCode == 200) {
            if (res.data != "") {
              //有信息
              _this.setData({
                userProfile: res.data,
                ExitOfTheUserinfo: true
              })
              getApp().globalData.userProfile = res.data; //设置全局变量
            } else {
              //无信息
              _this.setData({
                ExitOfTheUserinfo: false
              })
            }
          } else {
            console.log(res);
            util.go404Page(res.statusCode, res.data);
          }
        },
        fail: function(res) {
          console.log(res);
          util.go404Page(res.statusCode, res.data);
        },
        complete: function(res) {
          resolve();
          wx.hideNavigationBarLoading();
        }
      })
    })
  },

  /**
   * 点击修改信息
   */
  modifyUserInfo: function(event) {
    this.setData({
      ExitOfTheUserinfo: !this.data.ExitOfTheUserinfo
    })
  },

  /**
   * 退出修改信息
   */
  back: function (res) {
    this.setData({
      ExitOfTheUserinfo: !this.data.ExitOfTheUserinfo
    })
  },

  /**
   * 姓名
   */
  userName: function(event) {
    this.setData({
      name: event.detail.value
    })
  },
  
  /**
   * 性别
   */
  userGender: function(event) {
    this.setData({
      gender: event.detail.value
    })
  },
  
  /**
   * 学历
   */
  userEdu: function(event) {
    this.setData({
      edu: event.detail.value
    })
  },

  /**
   * 毕业院校
   */
  userSchool: function(event) {
    this.setData({
      school: event.detail.value
    })
  },
  
  /**
   * 专业
   */
  userMajor: function(event) {
    this.setData({
      major: event.detail.value
    })
  },
  
  /**
   * 毕业年份
   */
  userEduYear: function(event) {
    this.setData({
      graduateYear: event.detail.value
    })
  },
  
  /**
   * 邮箱
   */
  userEmail: function(event) {
    this.setData({
      email: event.detail.value
    })
  },

  /**
   * 保存信息
   */
  saveInfo: function(event) {
    var _this = this;
    //判断姓名格式
    if (!util.isName(_this.data.name)) {
      wx.showToast({
        title: '名字格式输入不正确',
        icon: "none",
        duration: 2000
      })
      return;
    }
    //判断性别格式
    if (!(_this.data.gender == '男' || _this.data.gender == '女')) {
      wx.showToast({
        title: '性别格式输入不正确',
        icon: "none",
        duration: 2000
      })
      return;
    }
    //判断学历格式
    if (!util.isName(_this.data.edu)) { //借用姓名的格式
      wx.showToast({
        title: '学历格式输入不正确',
        icon: "none",
        duration: 2000
      })
      return;
    }
    //判断学校格式
    if (!util.isSchool(_this.data.school)) {
      wx.showToast({
        title: '学校格式输入不正确',
        icon: "none",
        duration: 2000
      })
      return;
    }
    //判断专业格式
    if (_this.data.major == "") {
      wx.showToast({
        title: '专业格式输入不正确',
        icon: "none",
        duration: 2000
      })
      return;
    }
    //判断毕业年份格式
    if (!(_this.data.graduateYear == 2018 || _this.data.graduateYear == 2019 || _this.data.graduateYear == 2020 || _this.data.graduateYear == 2021 || _this.data.graduateYear == 2022)) {
      wx.showToast({
        title: '请输入2018-2022之间的年份',
        icon: "none",
        duration: 2000,
      })
      return;
    }
    //判断邮箱格式
    if (!util.isEmail(_this.data.email)) {
      wx.showToast({
        title: '邮箱格式输入不正确',
        icon: "none",
        duration: 2000
      })
      return;
    }
    wx.showLoading({
      title: '正在保存信息',
    })
    var param = new Object();
    if(_this.data.userProfile != null){
      param.id = _this.data.userProfile.id;
    }
    param.userLoginId = getApp().globalData.userLogin.id;
    param.name = this.data.name;
    param.gender = (this.data.gender == "男")? 1 : 0;
    param.edu = this.data.edu;
    param.school = this.data.school;
    param.major = this.data.major;
    param.graduateYear = this.data.graduateYear;
    param.email = this.data.email;
    wx.request({
      url: baseURLtest + 'user_profile',
      method: "POST",
      header: getApp().globalData.header,
      data: JSON.stringify(param),
      success: function(res) {
        if(res.statusCode == 200) {
          wx.hideLoading();
          wx.showToast({
            title: '保存信息成功！',
          })
          _this.getUserInfoDetail();
        }else{
          console.log(res);
          util.go404Page(res.statusCode, res.data);
        }
      },
      fail: function(res) {
        console.log(res);
        util.go404Page(res.statusCode, res.data);
      },
      complete: function(res) {
      }
    })
  },
})