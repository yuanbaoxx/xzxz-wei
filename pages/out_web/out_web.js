// pages/out_web/out_web.js
Page({

  /**
   * 页面的初始数据
   */
  data: {
    web_url: "",
    title: "",
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    console.log(options);
    this.setData({
      web_url: JSON.parse(decodeURIComponent(options.web_url)),
      title: options.title
    })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function (res) {
    var _this = this;
    if (res.from === 'button') {
    }
    return {
      title: _this.data.title,
      path: '/pages/out_web/out_web?web_url=' + encodeURIComponent(JSON.stringify(_this.data.web_url)) + "&&title=" + _this.data.title
    }
  }
})