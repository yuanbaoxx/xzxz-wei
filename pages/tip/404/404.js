// pages/tip/404/404.js
Page({

  /**
   * 页面的初始数据
   */
  data: {
    statusCode: '',
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function(options) {
    console.log(options);
    this.setData({
      statusCode: options.statusCode
    })
  },

  //返回首页
  backToFirst: function(e) {
    wx.switchTab({
      url: '/pages/campus/campus',
    })
  }
})