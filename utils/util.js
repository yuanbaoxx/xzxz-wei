//将对象转为时间
const formatTime = number => {
  var date = new Date(number);
  const year = date.getFullYear();
  const month = date.getMonth() + 1; 
  const day = date.getDate();
  const hour = date.getHours();
  const minute = date.getMinutes();
  const second = date.getSeconds();
  return [year, month, day].map(formatNumber).join('-') + ' ' + [hour, minute, second].map(formatNumber).join(':');
}

const formatDate = number => {
  var date = new Date(number);
  const year = date.getFullYear();
  const month = date.getMonth() + 1;
  const day = date.getDate();
  return [year, month, day].map(formatNumber).join('.');
}

const formatNumber = n => {
  n = n.toString()
  return n[1] ? n : '0' + n
}

//检测输入的手机号
function isMobilePhone(phonenumber){//已测试
  var regex = /^0?(13[0-9]|14[579]|15[012356789]|16[6]|17[0135678]|18[0-9]|19[89])[0-9]{8}$/;
  if(!regex.test(phonenumber)){
    return false;
  }else{
    return true;
  }
}

//检测输入的姓名
function isName(name){//已测试
  var regex = /^([\u4e00-\u9fa5]){2,7}$/;//只能是汉字，长度为2-7；
  if (!regex.test(name)) {
    return false;
  } else {
    return true;
  }
}

//判断输入的邮箱格式
function isEmail(email){//已测试
  var regex = /^[a-z0-9]+([._\\-]*[a-z0-9])*@([a-z0-9]+[-a-z0-9]*[a-z0-9]+.){1,63}[a-z0-9]+$/;
  if (!regex.test(email)) {
    return false;
  } else {
    return true;
  }
}

//判断输入的是否是汉字
function isSchool(str){//2-30位的汉字
  var regex = /^([\u4e00-\u9fa5]){3,20}$/;
  if (!regex.test(str)) {
    return false;
  } else {
    return true;
  }
}

//根据经纬度获取地址
function getMyCity(latitude, longitude, mapURL) {
  return new Promise(function (resolve, reject) {
    wx.request({
      url: mapURL + "?ak=f3uxQN89tHgF5f8UiMF4xBb8sMqczES2" + "&location=" + latitude + "," + longitude + "&output=json",
      success: function(res) {
        resolve(res);
      }
    })
  })
}

//将日期字符串加一天
function nextDayOfDateStr(dateStr) {
  console.log(dateStr);
  /**
   * 1-1 1-2 2-1 2-2
   */
  var str = datetime.split('');
  var day = parseInt(str[7]);
  var nextDay = day + 1;
  str[7] = nextDay + "";
  console.log(str.join(""));
}

/**
 * 跳转到404页面
 */
function go404Page(statusCode, err) {
  wx.reLaunch({
    url: '/pages/tip/404/404?statusCode=' + statusCode,
  })
}

module.exports = {
  isSchool: isSchool,
  isEmail: isEmail,
  isName: isName,
  isMobilePhone: isMobilePhone,
  formatTime: formatTime,
  formatDate: formatDate,
  getMyCity: getMyCity,
  go404Page: go404Page
}


